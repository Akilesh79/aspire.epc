﻿using Aspire.ePC;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using CoreCommon.Contract;
using CoreCommon.Helper;
using CoreCommon.TokenStore;
using Microsoft.Extensions.Logging;

[assembly: FunctionsStartup(typeof(StartUp))]
namespace Aspire.ePC
{
    class StartUp : FunctionsStartup
    {
        public IConfiguration _configuration;

        public override void Configure(IFunctionsHostBuilder builder)
        {
            string keyVaultEndpoint = Environment.GetEnvironmentVariable("KeyVaultEndpoint");

            var configbuilder = new ConfigurationBuilder();

            if (!string.IsNullOrEmpty(keyVaultEndpoint))
            {
                AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
                KeyVaultClient keyVaultClient = new KeyVaultClient(
                new KeyVaultClient.AuthenticationCallback(
                azureServiceTokenProvider.KeyVaultTokenCallback));
                configbuilder.AddAzureKeyVault(
                keyVaultEndpoint, keyVaultClient, new DefaultKeyVaultSecretManager());
            }
            _configuration = configbuilder.Build();

            builder.Services.AddScoped<IHttpClient, HttpClientHelper>();
            builder.Services.AddScoped<IOktaToken, OktaToken>();
            builder.Services.AddHttpClient();
            builder.Services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.SetMinimumLevel(LogLevel.Debug);
                loggingBuilder.SetMinimumLevel(LogLevel.Information);
                loggingBuilder.SetMinimumLevel(LogLevel.Error);
            });
        }
    }
}
