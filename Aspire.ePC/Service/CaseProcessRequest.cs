﻿using Aspire.ePC.Model;
using CoreCommon.Contract;
using Aspire.ePC.Model;
using Aspire.ePC.Model.GetCaseResponse;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Aspire.ePC.Model;
using Aspire.ePC.Model.Epc;
using CaseText1 = Aspire.ePC.Model.Epc.CaseText1;
using getcase = Aspire.ePC.Model.GetCaseResponse;

namespace Aspire.ePC.Service
{
    internal class CaseProcessRequest
    {
        public readonly string _json;
        public readonly string _clientID;
        private readonly IHttpClient _httpClient;
        private readonly IOktaToken _oktaClient;
        private readonly IDictionary<string, string> _headerDict;
        private readonly string _transactionType;
        public IConfiguration _configuration;
        public ILogger _log;
        public string _errorInfo;

        public CaseProcessRequest(string Json, string clientID, IHttpClient httpClient, string transactionType, IOktaToken oktaClient, IConfiguration configuration, ILogger log,IDictionary<string, string> reqHeaders = null)
        {
            _json = Json;
            _clientID = clientID;
            _httpClient = httpClient;
            _headerDict = reqHeaders;
            _transactionType = transactionType;
            _oktaClient = oktaClient;
            _configuration = configuration;
            _log = log;
        }

        public CaseProcessRequest(IHttpClient httpClient, IDictionary<string, string> reqHeaders, ILogger log, IConfiguration configuration = null)
        {
            _httpClient = httpClient;
            _headerDict = reqHeaders;
            _configuration = configuration;
            _log = log;
        }

        public async Task<object> ProcessCaseBody()
        {
            ePCCase requestData1 = JsonConvert.DeserializeObject<ePCCase>(_json);

            CreateCaseRequest requestData = new CreateCaseRequest();
            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();
            try
            {
                EpcCreateCase epcRequest = new EpcCreateCase
                {
                    UserName = _configuration["ePCaisuser"],
                    Password = _configuration["ePCPassword"]                    
                };
                Model.Epc.Case1[] caseRequestArray = new Model.Epc.Case1[1];
                Model.Epc.Case1 caseRequest = new Model.Epc.Case1
                {
                    case_id = string.IsNullOrEmpty(requestData1.caseID) ? "-1" : requestData1.caseID, //(_transactionType == HttpMethod.Post.ToString()) ? "-1" : requestData.caseId.ToString(),
                    company_id = requestData1.clientID,
                    address_id = requestData.addressId != 0 ? requestData.addressId.ToString() : null,
                    initial_user_code = requestData.initialUserCode,
                    responsible_user_code = requestData.responsibleUserCode,
                    case_status = requestData.caseStatus,
                    case_status_code = requestData.caseStatusCode,
                    received = requestData.received,
                    closed = requestData.closed,
                    total_work_time = Convert.ToInt32(requestData.totalWorkTime) != 0 ? requestData.totalWorkTime.ToString() : "",
                    contact_multiplier = Convert.ToInt32(requestData.contactMultiplier) != 0 ? requestData.contactMultiplier.ToString() : "",
                    incoming_phone = requestData.incomingPhone,
                    first_pick_date = requestData.firstPickDate,
                    first_pick_queue_user_code = requestData.firstPickQueueUserCode,
                    first_pick_user_code = requestData.firstPickUserCode,
                    first_pick_time = Convert.ToInt32(requestData.firstPickTime) != 0 ? requestData.firstPickTime.ToString() : "",
                    queue_time = Convert.ToInt32(requestData.queueTime) != 0 ? requestData.queueTime.ToString() : "",
                    queue_count = Convert.ToInt32(requestData.queueCount) != 0 ? requestData.queueCount.ToString() : "",
                    transfer_count = Convert.ToInt32(requestData.transferCount) != 0 ? requestData.transferCount.ToString() : "",
                    first_response_date = requestData.firstResponseDate,
                    first_response_time = Convert.ToInt32(requestData.firstResponseTime) != 0 ? requestData.firstResponseTime.ToString() : "",
                    first_response_user_code = requestData.firstResponseUserCode,
                    communication_count = Convert.ToInt32(requestData.communicationCount) != 0 ? requestData.communicationCount.ToString() : "",
                    closed_user_code = requestData.closedUserCode,
                    closed_time = Convert.ToInt32(requestData.closedTime) != 0 ? requestData.closedTime.ToString() : "",
                    pci_status = requestData.pciStatus,
                    case_address_role = requestData.caseAddressRole,
                    date_added = requestData.dateAdded,
                    added_by_user_code = requestData.addedByUserCode,
                    date_changed = requestData.dateChanged,
                    changed_by_user_code = requestData.changedByUserCode,
                    agent_assist = requestData.agentAssist,
                    identity_id = Convert.ToInt32(requestData.identityId) != 0 ? requestData.identityId.ToString() : "",
                    b05_code = requestData.b05Code,
                    b06_code = requestData.b06Code,
                    b07_code = requestData.b07Code,
                    b08_code = requestData.b08Code,
                    b09_code = requestData.b09Code,
                    b10_code = requestData.b10Code,
                    b11_code = requestData.b11Code,
                    b12_code = requestData.b12Code,
                    b13_code = requestData.b13Code,
                    b14_code = requestData.b14Code,
                    b15_code = requestData.b15Code,
                    b16_code = requestData.b16Code,
                    b17_code = requestData.b17Code,
                    b18_code = requestData.b18Code,
                    b19_code = requestData.b19Code,
                    b20_code = requestData.b20Code,
                    b21_code = requestData.b21Code,
                    b22_code = requestData.b22Code,
                    b23_code = requestData.b23Code,
                    b24_code = requestData.b24Code,
                    b25_code = requestData.b25Code,
                    b26_code = requestData.b26Code,
                    b27_code = requestData.b27Code,
                    b28_code = requestData.b28Code,
                    b29_code = requestData.b29Code,
                    b30_code = requestData.b30Code,
                    b31_code = requestData.b31Code,
                    b32_code = requestData.b32Code,
                    b33_code = requestData.b33Code,
                    b34_code = requestData.b34Code,
                    b35_code = requestData.b35Code,
                    b36_code = requestData.b36Code,
                    b37_code = requestData.b37Code,
                    b38_code = requestData.b38Code,
                    b39_code = requestData.b39Code,
                    b40_code = requestData.b40Code,
                    b41_code = requestData.b41Code,
                    b42_code = requestData.b42Code,
                    b43_code = requestData.b43Code,
                    b44_code = requestData.b44Code,
                    b45_code = requestData.b45Code,
                    b46_code = requestData.b46Code,
                    b47_code = requestData.b47Code,
                    b48_code = requestData.b48Code,
                    b49_code = requestData.b49Code
                };

                if (_transactionType == HttpMethod.Post.ToString() && !string.IsNullOrEmpty(requestData.b48Code))
                {
                    caseRequest.case_id = requestData.caseId.ToString();
                }
                if (requestData.issueList != null)
                {
                    Issuelist1 issueList = new Issuelist1
                    {
                        keys = "issue_seq"
                    };
                    Model.Epc.Issue1[] issueArray = new Model.Epc.Issue1[requestData.issueList.Count];
                    int i = 0;
                    foreach (Aspire.ePC.Model.IssueList requestissue in requestData.issueList)
                    {
                        Aspire.ePC.Model.Epc.Issue1 epcIssue = new Aspire.ePC.Model.Epc.Issue1
                        {
                            issue_seq = requestissue.issueSeq == 0 ? "-1" : requestissue.issueSeq.ToString(),
                            product_code = requestissue.productCode,
                            assigned_to_user_code = requestissue.assignedToUserCode,
                            issue_status = requestissue.issueStatus,
                            issue_status_code = requestissue.issueStatusCode,
                            closed = requestissue.closed,
                            issue_multiplier = requestissue.issueMultiplier.ToString(),
                            custom_date1 = requestissue.customDate1,
                            date_added = requestissue.dateAdded,
                            added_by_user_code = requestissue.addedByUserCode,
                            date_changed = requestissue.dateChanged,
                            changed_by_user_code = requestissue.changedByUserCode,
                            c05_code = requestissue.c05Code,
                            c06_code = requestissue.c06Code,
                            c07_code = requestissue.c07Code,
                            c08_code = requestissue.c08Code,
                            c09_code = requestissue.c09Code,
                            c10_code = requestissue.c10Code,
                            c11_code = requestissue.c11Code,
                            c12_code = requestissue.c12Code,
                            c13_code = requestissue.c13Code,
                            c14_code = requestissue.c14Code,
                            c15_code = requestissue.c15Code,
                            c16_code = requestissue.c16Code,
                            c17_code = requestissue.c17Code,
                            c18_code = requestissue.c18Code,
                            c19_code = requestissue.c19Code,
                            c20_code = requestissue.c20Code,
                            c21_code = requestissue.c21Code,
                            c22_code = requestissue.c22Code,
                            c23_code = requestissue.c23Code,
                            c24_code = requestissue.c24Code,
                            c25_code = requestissue.c25Code,
                            c26_code = requestissue.c26Code,
                            c27_code = requestissue.c27Code,
                            c28_code = requestissue.c28Code,
                            c29_code = requestissue.c29Code,
                            c30_code = requestissue.c30Code,
                            c31_code = requestissue.c31Code,
                            c32_code = requestissue.c32Code,
                            c33_code = requestissue.c33Code,
                            c34_code = requestissue.c34Code,
                            c35_code = requestissue.c35Code,
                            c36_code = requestissue.c36Code,
                            c37_code = requestissue.c37Code,
                            c38_code = requestissue.c38Code,
                            c39_code = requestissue.c39Code,
                            c40_code = requestissue.c40Code,
                            c41_code = requestissue.c41Code,
                            c42_code = requestissue.c42Code,
                            c43_code = requestissue.c43Code,
                            c44_code = requestissue.c44Code,
                            c45_code = requestissue.c45Code,
                            c46_code = requestissue.c46Code,
                            c47_code = requestissue.c47Code,
                            c48_code = requestissue.c48Code,
                            c49_code = requestissue.c49Code,
                            c50_code = requestissue.c50Code,
                            c51_code = requestissue.c51Code,
                            c52_code = requestissue.c52Code,
                            c53_code = requestissue.c53Code,
                            c54_code = requestissue.c54Code,
                            c55_code = requestissue.c55Code,
                            c56_code = requestissue.c56Code,
                            c57_code = requestissue.c57Code,
                            c58_code = requestissue.c58Code,
                            c59_code = requestissue.c59Code,
                            c60_code = requestissue.c60Code,
                            c61_code = requestissue.c61Code,
                            c62_code = requestissue.c62Code,
                            c63_code = requestissue.c63Code,
                            c64_code = requestissue.c64Code,
                            c65_code = requestissue.c65Code,
                            c66_code = requestissue.c66Code,
                            c67_code = requestissue.c67Code,
                            c68_code = requestissue.c68Code,
                            c69_code = requestissue.c69Code,
                            c70_code = requestissue.c70Code,
                            c71_code = requestissue.c71Code,
                            c72_code = requestissue.c72Code,
                            c73_code = requestissue.c73Code,
                            c74_code = requestissue.c74Code,
                            c75_code = requestissue.c75Code,
                            c76_code = requestissue.c76Code,
                            c77_code = requestissue.c77Code,
                            c78_code = requestissue.c78Code,
                            c79_code = requestissue.c79Code,
                            c80_code = requestissue.c80Code,
                            c81_code = requestissue.c81Code,
                            c82_code = requestissue.c82Code,
                           
                            c84_code = requestissue.c84Code,
                            c85_code = requestissue.c85Code,
                            c86_code = requestissue.c86Code,
                            c87_code = requestissue.c87Code,
                           
                            c89_code = requestissue.c89Code,
                            c90_code = requestissue.c90Code,
                            c91_code = requestissue.c91Code,
                            c92_code = requestissue.c92Code,
                            c93_code = requestissue.c93Code,
                            c94_code = requestissue.c94Code,
                            c95_code = requestissue.c95Code,
                            c96_code = requestissue.c96Code,
                            c97_code = requestissue.c97Code,
                            c98_code = requestissue.c98Code,
                            c99_code = requestissue.c99Code
                        };
                        if (String.IsNullOrEmpty(requestissue.c83Code) && String.IsNullOrEmpty(requestissue.c88Code))
                        {
                            DateTime dtutc = DateTime.UtcNow;
                            epcIssue.c83_code = dtutc.ToString("yyyy-MM-dd HH:mm:ss"); //Actual ResponseTime/Expected ResponseTime
                            epcIssue.c88_code = dtutc.AddMinutes(1).ToString("yyyy-MM-dd HH:mm:ss"); //PromiseTime
                        }
                        else
                        {
                            epcIssue.c83_code = requestissue.c83Code;
                            epcIssue.c88_code = requestissue.c88Code;
                        }
                        issueArray[i] = epcIssue;
                        i++;
                    }

                    issueList.Issue = issueArray;
                    caseRequest.IssueList = issueList;
                }
                if (requestData.addressList != null)
                {
                    Model.Epc.Addresslist1 addressList = new Model.Epc.Addresslist1
                    {
                        keys = "address_id"
                    };

                    Model.Epc.Address1[] addressArray = new Model.Epc.Address1[requestData.addressList.Count];

                    Aspire.ePC.Model.Epc.Address1 epcAddress = null;
                    foreach (Aspire.ePC.Model.AddressList requestAddress in requestData.addressList)
                    {
                        if (requestAddress.addressId != -1)
                        {
                            epcAddress = new Aspire.ePC.Model.Epc.Address1
                            {
                                address_id = requestAddress.addressId.ToString()
                            };
                        }
                        else
                        {
                            epcAddress = new Aspire.ePC.Model.Epc.Address1
                            {
                                address_id = requestAddress.addressId.ToString(),
                                address_code = requestAddress.addressCode,
                                active = requestAddress.active == true ? "Y" : "N",
                                account_number = requestAddress.accountNumber,
                                name_title = requestAddress.nameTitle,
                                given_names = requestAddress.givenNames,
                                middle_initial = requestAddress.middleInitial != 0 ? requestAddress.middleInitial.ToString() : null,
                                last_name = requestAddress.lastName,
                                suffix = requestAddress.suffix,
                                company_name = requestAddress.companyName,
                                job_title = requestAddress.jobTitle,
                                address1 = requestAddress.address1,
                                address2 = requestAddress.address2,
                                address3 = requestAddress.address3,
                                city = requestAddress.city,
                                state = requestAddress.state,
                                postal_code = requestAddress.postalCode,
                                country = requestAddress.country,
                                email = requestAddress.email,
                                email2 = requestAddress.email2,
                                email3 = requestAddress.email3,
                                search_name = requestAddress.searchName,
                                search_address = requestAddress.searchAddress,
                                originated_via = requestAddress.originatedVia,
                                originated_date = requestAddress.originatedDate,
                                last_modified = requestAddress.lastModified,
                                allow_survey = requestAddress.allowSurvey == true ? "Y" : "N",
                                last_survey = requestAddress.lastSurvey,
                                last_contact = requestAddress.lastContact,
                                accumulated_goodwill = requestAddress.accumulatedGoodwill.ToString(),
                                where_to_buy = requestAddress.whereToBuy,
                                latitude = requestAddress.latitude != 0.00 ? requestAddress.latitude.ToString() : null,
                                longitude = requestAddress.longitude != 0.00 ? requestAddress.longitude.ToString() : null,
                                instructions = requestAddress.instructions,
                                repeater_code = requestAddress.repeaterCode,
                                encl_auth_level = requestAddress.enclAuthLevel.ToString(),
                                county = requestAddress.county,
                                opt_out = requestAddress.optOut.ToString(),
                                language_id = requestAddress.languageId,
                                currency_code = requestAddress.currencyCode,
                                primary_address_id = requestAddress.primaryAddressId,
                                date_added = requestAddress.dateAdded,
                                added_by_user_code = requestAddress.addedByUserCode,
                                date_changed = requestAddress.dateChanged,
                                changed_by_user_code = requestAddress.changedByUserCode,
                                a05_code = requestAddress.a05Code,
                                a06_code = requestAddress.a06Code,
                                a07_code = requestAddress.a07Code,
                                a08_code = requestAddress.a08Code,
                                a09_code = requestAddress.a09Code,
                                a10_code = requestAddress.a10Code,
                                a11_code = requestAddress.a11Code,
                                a12_code = requestAddress.a12Code,
                                a13_code = requestAddress.a13Code,
                                a14_code = requestAddress.a14Code,
                                a15_code = requestAddress.a15Code,
                                a16_code = requestAddress.a16Code,
                                a17_code = requestAddress.a17Code,
                                a18_code = requestAddress.a18Code,
                                a19_code = requestAddress.a19Code,
                                a20_code = requestAddress.a20Code,
                                a21_code = requestAddress.a21Code,
                                a22_code = requestAddress.a22Code,
                                a23_code = requestAddress.a23Code,
                                a24_code = requestAddress.a24Code,
                                a25_code = requestAddress.a25Code,
                                a26_code = requestAddress.a26Code,
                                a27_code = requestAddress.a27Code,
                                a28_code = requestAddress.a28Code,
                                a29_code = requestAddress.a29Code,
                                a30_code = requestAddress.a30Code,
                                a31_code = requestAddress.a31Code,
                                a32_code = requestAddress.a32Code,
                                a33_code = requestAddress.a33Code,
                                a34_code = requestAddress.a34Code,
                                a35_code = requestAddress.a35Code,
                                a36_code = requestAddress.a36Code,
                                a37_code = requestAddress.a37Code,
                                a38_code = requestAddress.a38Code,
                                a39_code = requestAddress.a39Code,
                                a40_code = requestAddress.a40Code,
                                a41_code = requestAddress.a41Code,
                                a42_code = requestAddress.a42Code,
                                a43_code = requestAddress.a43Code,
                                a44_code = requestAddress.a44Code,
                                a45_code = requestAddress.a45Code,
                                a46_code = requestAddress.a46Code,
                                a47_code = requestAddress.a47Code,
                                a48_code = requestAddress.a48Code,
                                a49_code = requestAddress.a49Code,
                                a50_code = requestAddress.a50Code,
                                a51_code = requestAddress.a51Code,
                                a52_code = requestAddress.a52Code,
                                a53_code = requestAddress.a53Code,
                                a54_code = requestAddress.a54Code,
                                a55_code = requestAddress.a55Code,
                                a56_code = requestAddress.a56Code,
                                a57_code = requestAddress.a57Code,
                                a58_code = requestAddress.a58Code,
                                a59_code = requestAddress.a59Code,
                                a60_code = requestAddress.a60Code,
                                a61_code = requestAddress.a61Code,
                                a62_code = requestAddress.a62Code,
                                a63_code = requestAddress.a63Code,
                                a64_code = requestAddress.a64Code,
                                a65_code = requestAddress.a65Code,
                                a66_code = requestAddress.a66Code,
                                a67_code = requestAddress.a67Code,
                                a68_code = requestAddress.a68Code,
                                a69_code = requestAddress.a69Code,
                                a70_code = requestAddress.a70Code,
                                a71_code = requestAddress.a71Code,
                                a72_code = requestAddress.a72Code,
                                a73_code = requestAddress.a73Code,
                                a74_code = requestAddress.a74Code,
                                a75_code = requestAddress.a75Code,
                                a76_code = requestAddress.a76Code,
                                a77_code = requestAddress.a77Code,
                                a78_code = requestAddress.a78Code,
                                a79_code = requestAddress.a79Code,
                                a80_code = requestAddress.a80Code
                            };

                            if (requestData.addressList[0].phoneList != null)
                            {
                                Phonelist1 phoneList = new Phonelist1();
                                Phone1[] phoneArray = new Phone1[requestData.addressList[0].phoneList.Count];
                                int p = 0;
                                foreach (Aspire.ePC.Model.PhoneList phone in requestData.addressList[0].phoneList)
                                {
                                    Aspire.ePC.Model.Epc.Phone1 phoneEpc = new Aspire.ePC.Model.Epc.Phone1
                                    {
                                        address_phone_seq = phone.addressPhoneSeq,
                                        phone_type_code = phone.phoneTypeCode,
                                        phone = phone.phone
                                    };

                                    phoneArray[p] = phoneEpc;
                                    p++;
                                }
                                phoneList.Phone = phoneArray;

                                epcAddress.PhoneList = phoneList;
                            }
                        }
                        addressArray[0] = epcAddress;
                        addressList.Address = addressArray;
                    }
                    caseRequest.addressList = addressList;
                }
                if (requestData.caseTextList != null)
                {
                    Model.Epc.CaseTextList1 caseTextList = new Model.Epc.CaseTextList1
                    {
                       
                         keys = "case_text_seq"
                    };
                    CaseText1[] caseTextArray = new CaseText1[requestData.caseTextList.caseText.Length];
                    int c = 0;

                    foreach (Model.CaseText1 requestcaseTextList in requestData.caseTextList.caseText)
                    {
                        Aspire.ePC.Model.Epc.CaseText1 epccaseTextList = new Aspire.ePC.Model.Epc.CaseText1
                        {
                            case_text_seq = (_transactionType == HttpMethod.Post.ToString() ? "-1" : requestcaseTextList.case_text_seq?.ToString()),

                            action_seq = requestcaseTextList.action_seq?.ToString(),
                            issue_seq = requestcaseTextList.issue_seq?.ToString(),
                            description = requestcaseTextList.description,
                            text_type_code = requestcaseTextList.text_type_code,
                            case_text = requestcaseTextList.case_text
                        };
                        caseTextArray[c] = epccaseTextList;
                        c++;
                    }
                    caseTextList.caseText = caseTextArray;
                    caseRequest.caseTextList = caseTextList;
                    }

                    caseRequestArray[0] = caseRequest;
                epcRequest.Case = caseRequestArray;

                Model.Epc.Responseformat response = new Model.Epc.Responseformat();
                Model.Epc.Caselist caseListresponse = new Model.Epc.Caselist();

                Model.Epc.Case cresponse = new Model.Epc.Case
                {
                    AllAttributes = "Item1"
                };

                Model.Epc.Addresslist addressListResponse = new Model.Epc.Addresslist();
                Model.Epc.Address addressResponse = new Model.Epc.Address
                {
                    AllAttributes = "Item1"
                };

                Model.Epc.Phonelist[] phoneArrayResponse = new Model.Epc.Phonelist[1];
                Model.Epc.Phonelist phoneResponse = new Model.Epc.Phonelist();
                Model.Epc.Phone presponse = new Model.Epc.Phone
                {
                    AllAttributes = "Item1"
                };
                phoneArrayResponse[0] = phoneResponse;
                phoneArrayResponse[0].Phone = presponse;

                addressResponse.PhoneList = phoneArrayResponse;
                addressListResponse.Address = addressResponse;


                Casetextlist caseListResponse = new Casetextlist();

                Casetext caseTextResponse = new Casetext
                {
                    AllAttributes = "Item1"
                };

                caseListResponse.CaseText = caseTextResponse;



                Issuelist issueListResponse = new Issuelist();

                Issue issueResponse = new Issue
                {
                    AllAttributes = "Item1"
                };

                issueListResponse.Issue = issueResponse;

                cresponse.AddressList = addressListResponse;
                cresponse.CaseTextList = caseListResponse;
                cresponse.IssueList = issueListResponse;

                caseListresponse.Case = cresponse;

                response.CaseList = caseListresponse;

                epcRequest.ResponseFormat = response;

                string caseCreateRequest = JsonConvert.SerializeObject(epcRequest,
      Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                string url = _configuration["epcCreateCaseURL"];
                HttpResponseMessage responseEPC = await _httpClient.SendAsync(_configuration["epcCreateCaseURL"], caseCreateRequest, _headerDict, "POST");


                epcErrorResponse = EPCErrorResponse(responseEPC);

                if (!string.IsNullOrEmpty(epcErrorResponse.errorCode))
                {
                    _log.LogInformation("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessCaseBody " + " Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);
                    _errorInfo = SerializeErrorInformation(_clientID, epcErrorResponse.errorCode, epcErrorResponse.message, Environment.GetEnvironmentVariable("SelfServeRequestType") + " " + _transactionType + " " + Environment.GetEnvironmentVariable("ErrorSelfserveCase"));
                    _log.LogInformation(Environment.GetEnvironmentVariable("epcDown") + "" + _errorInfo);
                    // Log Report
                    LogReportsRequestData(_headerDict["X-Correlation-Id"]?.ToString(), _headerDict["X-Program-Id"]?.ToString(), "Failure", "CREATE_SELFSERVE_CASE", "", epcErrorResponse.errorCode, epcErrorResponse.message);

                    return epcErrorResponse;

                }              

                if (responseEPC.StatusCode == HttpStatusCode.OK)
                {
                    string responseContent = await responseEPC.Content.ReadAsStringAsync();
                    responseContent = responseContent.Replace("Field", "");
                    EpcCreateCase responsePayload = JsonConvert.DeserializeObject<EpcCreateCase>(responseContent);

                    _log.LogInformation("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessCaseBody " + " Message:  " + responsePayload.ToString());

                    CreateCaseRequest createCaseResponse = (CreateCaseRequest)FormatCaseResponse(responsePayload);

                    _log.LogDebug("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessCaseBody " + " Message:  " + createCaseResponse.ToString());

                    return createCaseResponse;
                }

            }
            catch (Exception ex)
            {

                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = ex.Message.ToString();
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;
                _log.LogError("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessCaseBody " + " Error Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);

                // Log Report
                LogReportsRequestData(_headerDict["X-Correlation-Id"]?.ToString(), _headerDict["X-Program-Id"]?.ToString(), "Failure", "CREATE_SELFSERVE_CASE", "", epcErrorResponse.errorCode, Environment.GetEnvironmentVariable("epcServicenotfoundMessage"));

                return epcErrorResponse;
            }

            return epcErrorResponse;
        }

        public string SerializeErrorInformation(string clientId, string errorCode, string errorMessage, string endPoint)
        {
            ErrorInformation errorInformation = new ErrorInformation();

            errorInformation.errorCode = errorCode;
            errorInformation.errorMessage = errorMessage;
            errorInformation.clientId = clientId;
            errorInformation.endPoint = endPoint;

            string information = JsonConvert.SerializeObject(errorInformation);

            return information;
        }

        public async Task LogReportsRequestData(string correlationId, string programId, string status, string source, string successMessage, string errorCode, string errorDescription)
        {
            try
            {
                CreateCaseRequest requestData = JsonConvert.DeserializeObject<CreateCaseRequest>(_json);

                ReportRequest reportRequest = new ReportRequest();
                Requestdetails requestdetails = new Requestdetails();
                Error error = new Error();
                Successmessage successmessage = new Successmessage();
                Customer customer = new Customer();
                Verificationdata verificationdata = new Verificationdata();
                Aspire.ePC.Model.Case caseObj = new Aspire.ePC.Model.Case();

                reportRequest.correlationId = correlationId;
                reportRequest.partyId = "";
                reportRequest.source = source;
                reportRequest.organization = _clientID;
                reportRequest.programId = programId;
                reportRequest.requestedDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                reportRequest.status = "INITIATED";

                verificationdata.verificationData = "";

                customer.firstName = "";
                customer.lastName = "";
                customer.userName = "";

                requestdetails.customer = customer;

                caseObj.apiReferenceId = requestData.b19Code;
                caseObj.requestDetails = "";
                caseObj.subClient = "";
                caseObj.caseType = "";
                requestdetails._case = caseObj;

                customer.verificationData = verificationdata;

                error.errorCode = errorCode;
                error.errorDescription = errorDescription;
                requestdetails.error = error;

                if (status == "Success")
                {
                    successmessage.message = successMessage;
                }
                else
                {
                    successmessage.message = "";
                }

                requestdetails.successMessage = successmessage;

                requestdetails.requestDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                requestdetails.status = status;
                reportRequest.requestDetails = requestdetails;

                IDictionary<string, string> headerDict = new Dictionary<string, string>();

                var request = JsonConvert.SerializeObject(reportRequest);

                var finalRequest = request.Replace("_case", "case");

                string reportsUrl = Environment.GetEnvironmentVariable("ReportsUrl");

                // _httpClient.SendAsync(reportsUrl, finalRequest, headerDict, false);

                 var status1 = await _httpClient.SendAsync(reportsUrl, finalRequest, headerDict, false);

            }
            catch (Exception ex)
            {
                _log.LogInformation("LogReportsRequestData " + ex.Message + " " + ex.InnerException.Message);
            }
        }

        public string DelayResponseTime(string input)
        {
            DateTime dt = DateTime.Parse(input);
            return dt.AddMinutes(1).ToString("yyyy-MM-dd HH:mm:ss");
        }

        public async Task<object> SearchCase(string clientId, string caseId, string b19code, string a62code)
        {
            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();
            try
            {
                GetCaseResponse caseRequest = new GetCaseResponse
                {
                    UserName = _configuration["ePCaisuser"],
                    Password = _configuration["ePCPassword"]
                };
                getcase.Case c = new getcase.Case
                {
                    company_id = !string.IsNullOrEmpty(clientId) ? clientId : null,
                    case_id = !string.IsNullOrEmpty(caseId) ? caseId : null,
                    b19_code = !string.IsNullOrEmpty(b19code) ? b19code : null

                };

                getcase.Addresslist buildAddressList = new getcase.Addresslist();
                getcase.Address[] addressArray = new getcase.Address[1];
                getcase.Address buildAddress = new getcase.Address();
                buildAddress.a62_code = !string.IsNullOrEmpty(a62code) ? a62code : null;

                addressArray[0] = buildAddress;
                buildAddressList.Address = addressArray;

                c.AddressList = buildAddressList;

                caseRequest.Case = c;

                getcase.Responseformat epcGetResponse = new getcase.Responseformat();

                getcase.Caselist caselist = new getcase.Caselist();

                getcase.Case1 responseCase = new getcase.Case1();

                getcase.Addresslist1 addressResponse = new getcase.Addresslist1();

                getcase.Address1 address = new getcase.Address1();

                getcase.Phonelist[] phoneListResponse = new getcase.Phonelist[1];

                getcase.Phonelist phonelistResponse = new getcase.Phonelist();
                getcase.CaseTextList caseTextList = new getcase.CaseTextList();
                caseTextList.CaseText = new CaseText
                { AllAttributes = "Item1"  };

                getcase.Phone phone = new getcase.Phone
                {
                    AllAttributes = "Item1"
                };

                phonelistResponse.Phone = phone;

                phoneListResponse[0] = phonelistResponse;

                phoneListResponse[0].Phone = phone;

                address.PhoneList = phoneListResponse;

                address.AllAttributes = "Item1";

                addressResponse.Address = address;

                responseCase.AllAttributes = "Item1";

                responseCase.AddressList = addressResponse;
                responseCase.CaseTextList = caseTextList;


                getcase.IssueList1 issueListResponse = new getcase.IssueList1();

                getcase.Issue1 issueResponse = new getcase.Issue1
                {

                    AllAttributes = "Item1"
                };

                issueListResponse.Issue = issueResponse;
                responseCase.IssueList = issueListResponse;

                caselist.Case = responseCase;
                epcGetResponse.CaseList = caselist;
                caseRequest.ResponseFormat = epcGetResponse;

                string caseCreateRequest = JsonConvert.SerializeObject(caseRequest,
      Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                string url = _configuration["epcSearchCaseURL"];

                HttpResponseMessage responseEPC = await _httpClient.SendAsync(_configuration["epcSearchCaseURL"], caseCreateRequest, _headerDict);

                epcErrorResponse = EPCErrorResponse(responseEPC);

                _log.LogInformation("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: SearchCase " + " Message:  " + epcErrorResponse.message);

                if (!string.IsNullOrEmpty(epcErrorResponse.errorCode))
                {
                    _errorInfo = SerializeErrorInformation(clientId, epcErrorResponse.errorCode, epcErrorResponse.message, Environment.GetEnvironmentVariable("ErrorSelfserveSearchCase"));
                    _log.LogInformation(Environment.GetEnvironmentVariable("epcDown") + "" + _errorInfo);
                    return epcErrorResponse;

                }

                string responseContent = await responseEPC.Content.ReadAsStringAsync();

                responseContent = responseContent.Replace("Field", "");

                _log.LogInformation("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: SearchCase " + " Message:  " + responseContent.ToString());

                var reposneType = JToken.Parse(responseContent);
                List<CreateCaseRequest> reponseModifiedList = new List<CreateCaseRequest>();

                if (reposneType is JArray)
                {

                    List<EpcCreateCase> responsePayloadList = JsonConvert.DeserializeObject<List<EpcCreateCase>>(responseContent);
                    foreach (EpcCreateCase responsePayload in responsePayloadList)
                    {
                        CreateCaseRequest createCaseResponse = (CreateCaseRequest)FormatCaseResponse(responsePayload);
                        reponseModifiedList.Add(createCaseResponse);
                    }
                }
                else if (reposneType is JObject)
                {
                    EpcCreateCase responsePayloadList = JsonConvert.DeserializeObject<EpcCreateCase>(responseContent);
                    int i = 0;

                    if (responsePayloadList.Case != null)
                    {
                        foreach (Model.Epc.Case1 responsePayload in responsePayloadList.Case)
                        {
                            JToken specialFields = reposneType["case"][i];
                            if (responsePayload?.addressList?.Address?[0].a62_code?.ToLower() == a62code?.ToLower())
                            {
                                CreateCaseRequest createCaseResponse = (CreateCaseRequest)FormatSearchCaseResponse(responsePayload, specialFields);
                                reponseModifiedList.Add(createCaseResponse);
                            }
                            i++;
                        }
                    }
                }
                _log.LogDebug("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: SearchCase " + " Message:  " + reponseModifiedList.ToString());
                return reponseModifiedList;

            }
            catch (Exception ex)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = ex.Message.ToString();
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;
                _log.LogError("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: SearchCase " + " Error Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);
                return epcErrorResponse;
            }


        }

        public EpcErrorResponse EPCErrorResponse(HttpResponseMessage responseEPC)
        {

            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();

            if (responseEPC.StatusCode == HttpStatusCode.NotFound)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = Environment.GetEnvironmentVariable("epcServicenotfoundMessage");
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;

            }
            else if (responseEPC.StatusCode == HttpStatusCode.Forbidden)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_FORBIDDEN_000");
                epcErrorResponse.message = "";
                epcErrorResponse.httpstatuscode = HttpStatusCode.Forbidden;

            }
            else if (responseEPC.StatusCode == HttpStatusCode.InternalServerError)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = "";
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;

            }           

            return epcErrorResponse;
        }

        public async Task<object> ProcessGetCase(string clientId, string caseId)
        {
            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();
            try
            {
                GetCaseResponse caseRequest = new GetCaseResponse
                {
                    UserName = _configuration["ePCaisuser"],
                    Password = _configuration["ePCPassword"],
                    Type = Environment.GetEnvironmentVariable("GetUnique")
                };
                getcase.Case c = new getcase.Case
                {
                    company_id = clientId,
                    case_id = caseId
                };
                caseRequest.Case = c;

                getcase.Responseformat epcGetResponse = new getcase.Responseformat();

                getcase.Caselist caselist = new getcase.Caselist();

                getcase.Case1 responseCase = new getcase.Case1();

                getcase.Addresslist1 addressResponse = new getcase.Addresslist1();

                getcase.Address1 address = new getcase.Address1();

                getcase.Phonelist[] phoneListResponse = new getcase.Phonelist[1];

                getcase.Phonelist phonelistResponse = new getcase.Phonelist();


                getcase.Phone phone = new getcase.Phone
                {
                    AllAttributes = "Item1"
                };
                getcase.CaseTextList caseTextList = new getcase.CaseTextList();
                caseTextList.CaseText = new CaseText
                { AllAttributes = "Item1" };

                phonelistResponse.Phone = phone;

                phoneListResponse[0] = phonelistResponse;

                phoneListResponse[0].Phone = phone;

                address.PhoneList = phoneListResponse;

                address.AllAttributes = "Item1";

                addressResponse.Address = address;

                responseCase.AllAttributes = "Item1";

                responseCase.AddressList = addressResponse;

                responseCase.CaseTextList = caseTextList;

                getcase.IssueList1 issueListResponse = new getcase.IssueList1();

                getcase.Issue1 issueResponse = new getcase.Issue1
                {

                    //  getcase.IssueDetailList[] issueDatailArrayResponse = new getcase.IssueDetailList[1];

                    // getcase.IssueDetailList issueDeatailResponse = new getcase.IssueDetailList();


                    //getcase.IssueDetail issueDetail = new getcase.IssueDetail
                    //{
                    //    AllAttributes = "Item1"
                    //};

                    //  issueDeatailResponse.IssueDetail = issueDetail;

                    //  issueDatailArrayResponse[0] = issueDeatailResponse;

                    //  issueDatailArrayResponse[0].IssueDetail = issueDetail;

                    // issueResponse.IssueDetailList = issueDatailArrayResponse;

                    AllAttributes = "Item1"
                };

                issueListResponse.Issue = issueResponse;


                responseCase.IssueList = issueListResponse;


                caselist.Case = responseCase;

                epcGetResponse.CaseList = caselist;

                caseRequest.ResponseFormat = epcGetResponse;


                string caseCreateRequest = JsonConvert.SerializeObject(caseRequest,
      Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                HttpResponseMessage responseEPC = await _httpClient.SendAsync(_configuration["epcGetCase"], caseCreateRequest, _headerDict);


                string responseContent = await responseEPC.Content.ReadAsStringAsync();

                responseContent = responseContent.Replace("Field", "");
                EpcCreateCase responsePayload = JsonConvert.DeserializeObject<EpcCreateCase>(responseContent);
                _log.LogDebug("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessGetCase " + " Message:  " + responsePayload.ToString());

                CreateCaseRequest createCaseResponse = (CreateCaseRequest)FormatCaseResponse(responsePayload);

                _log.LogDebug("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessGetCase " + " Message:  " + createCaseResponse.ToString());

                return createCaseResponse;

            }
            catch (Exception ex)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = ex.Message;
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;
                _log.LogError("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessGetCase " + " Error Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);
                return epcErrorResponse;

            }
        }

        public string formatDateTime(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                DateTime dt = DateTime.Parse(value);

                return dt.ToString("yyyy-MM-dd'T'HH:mm:ss");
            }
            return value;

        }

        public object FormatSearchCaseResponse(Model.Epc.Case1 responsePayload, JToken specialFields)
        {
            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();
            try
            {
                if (responsePayload != null)
                {
                    Model.Epc.Case1 responseCase = responsePayload;

                    CreateCaseRequest response = new CreateCaseRequest
                    {
                        companyId = responseCase.company_id,
                        caseId = Convert.ToInt32(responseCase.case_id),
                        initialUserCode = responseCase.initial_user_code,
                        responsibleUserCode = responseCase.responsible_user_code,
                        addressId = Convert.ToInt32(responseCase.address_id)
                    };
                    // response.caseStatus = "O" as =enum when responseCase.case_status == 'O' or responseCase.case_status == 'Open' otherwise "C" as =enum default null;
                    if (responseCase.case_status == "O" || responseCase.case_status == "Open")
                    {
                        response.caseStatus = "O";
                    }
                    else
                    {
                        response.caseStatus = "C";
                    }//need to recheck enum data type

                    response.caseStatusCode = responseCase.case_status_code;

                    if (responseCase.received != null)
                    {

                        response.received = formatDateTime(responseCase.received);
                    }

                    if (responseCase.closed != null)
                    {
                        response.closed = formatDateTime(responseCase.closed);

                    }


                    if (responseCase.total_work_time != null)
                    {

                        response.totalWorkTime = Convert.ToInt32(responseCase.total_work_time);
                    }

                    if (responseCase.contact_multiplier != null)
                    {
                        response.contactMultiplier = Convert.ToInt32(responseCase.contact_multiplier);

                    }

                    response.incomingPhone = responseCase.incoming_phone;
                    response.firstPickDate = responseCase.first_pick_date;
                    response.firstPickQueueUserCode = responseCase.first_pick_queue_user_code;
                    response.firstPickUserCode = responseCase.first_pick_user_code;

                    if (responseCase.first_pick_time != null)
                    {

                        response.firstPickTime = Convert.ToInt32(responseCase.first_pick_time);
                    }

                    //  response.queueTime = Convert.ToInt32(responseCase.queue_time);
                    if (responseCase.queue_time != null)
                    {
                        response.queueTime = Convert.ToInt32(responseCase.queue_time);
                    }

                    if (responseCase.queue_count != null)
                    {
                        response.queueCount = Convert.ToInt32(responseCase.queue_count);
                    }

                    if (responseCase.transfer_count != null)
                    {
                        response.transferCount = Convert.ToInt32(responseCase.transfer_count);
                    }


                    response.firstResponseDate = responseCase.first_response_date;

                    if (responseCase.first_response_time != null)
                    {
                        response.firstResponseTime = Convert.ToInt32(responseCase.first_response_time);
                    }

                    response.firstResponseUserCode = responseCase.first_response_user_code;
                    if (responseCase.communication_count != null)
                    {
                        response.communicationCount = Convert.ToInt32(responseCase.communication_count);
                    }

                    response.closedUserCode = responseCase.closed_user_code;

                    if (responseCase.closed_time != null)
                    {
                        response.closedTime = Convert.ToInt32(responseCase.closed_time);
                    }

                    response.pciStatus = responseCase.pci_status;
                    response.caseAddressRole = responseCase.case_address_role;

                    if (responseCase.date_added != null)
                    {
                        response.dateAdded = formatDateTime(responseCase.date_added);
                    }

                    response.addedByUserCode = responseCase.added_by_user_code;

                    if (responseCase.date_changed != null)
                    {
                        response.dateChanged = formatDateTime(responseCase.date_changed);
                    }


                    response.changedByUserCode = responseCase.changed_by_user_code;
                    response.agentAssist = responseCase.agent_assist;

                    if (responseCase.identity_id != null)
                    {
                        response.identityId = Convert.ToInt32(responseCase.identity_id);
                    }
                    else
                    {
                        if (specialFields["anyAttr"] != null)
                        {
                            JArray findIdentity = (JArray)specialFields["anyAttr"];

                            foreach (var key in findIdentity)
                            {
                                if (key["@identity_id"] != null)
                                {

                                    response.identityId = Convert.ToInt32(key["@identity_id"]);
                                    break;
                                }

                            }
                        }
                    }
                    response.b05Code = responseCase.b05_code;
                    response.b06Code = responseCase.b06_code;
                    response.b07Code = responseCase.b07_code;
                    response.b08Code = responseCase.b08_code;
                    response.b09Code = responseCase.b09_code;
                    response.b10Code = responseCase.b10_code;
                    response.b11Code = responseCase.b11_code;
                    response.b12Code = responseCase.b12_code;
                    response.b13Code = responseCase.b13_code;
                    response.b14Code = responseCase.b14_code;
                    response.b15Code = responseCase.b15_code;
                    response.b16Code = responseCase.b16_code;
                    response.b17Code = responseCase.b17_code;
                    response.b18Code = responseCase.b18_code;
                    response.b19Code = responseCase.b19_code;
                    response.b20Code = responseCase.b20_code;
                    response.b21Code = responseCase.b21_code;
                    response.b22Code = responseCase.b22_code;
                    response.b23Code = responseCase.b23_code;
                    response.b24Code = responseCase.b24_code;
                    response.b25Code = responseCase.b25_code;
                    response.b26Code = responseCase.b26_code;
                    response.b27Code = responseCase.b27_code;
                    response.b28Code = responseCase.b28_code;
                    response.b29Code = responseCase.b29_code;
                    response.b30Code = responseCase.b30_code;
                    response.b31Code = responseCase.b31_code;
                    response.b32Code = responseCase.b32_code;
                    response.b33Code = responseCase.b33_code;
                    response.b34Code = responseCase.b34_code;
                    response.b35Code = responseCase.b35_code;
                    response.b36Code = responseCase.b36_code;
                    response.b37Code = responseCase.b37_code;
                    response.b38Code = responseCase.b38_code;
                    response.b39Code = responseCase.b39_code;
                    response.b40Code = responseCase.b40_code;
                    response.b41Code = responseCase.b41_code;
                    response.b42Code = responseCase.b42_code;
                    response.b43Code = responseCase.b43_code;
                    response.b44Code = responseCase.b44_code;
                    response.b45Code = responseCase.b45_code;
                    response.b46Code = responseCase.b46_code;
                    response.b47Code = responseCase.b47_code;
                    response.b48Code = responseCase.b48_code;
                    response.b49Code = responseCase.b49_code;

                    if (responseCase.IssueList != null)
                    {
                        if (responseCase.IssueList.Issue != null)
                        {
                            response.issueList = new List<IssueList>();
                            foreach (Model.Epc.Issue1 issueResponse in responseCase.IssueList.Issue)
                            {
                                IssueList issueList = new IssueList
                                {
                                    issueSeq = Convert.ToInt32(issueResponse.issue_seq),
                                    productCode = issueResponse.product_code,
                                    assignedToUserCode = issueResponse.assigned_to_user_code
                                };
                                //issueList.issueStatus = "O" as =enum when issueResponse.issue_status == 'O' or issueResponse.issue_status == 'Open' otherwise "C" as =enum default null;
                                if (issueResponse.issue_status == "O" || issueResponse.issue_status == "Open")
                                {
                                    issueList.issueStatus = "O";
                                }
                                else
                                {
                                    issueList.issueStatus = "C";
                                }

                                issueList.issueStatusCode = issueResponse.issue_status_code;
                                if (issueResponse.closed != null)
                                {
                                    issueList.closed = formatDateTime(issueResponse.closed);

                                }


                                if (issueResponse.issue_multiplier != null)
                                {
                                    issueList.issueMultiplier = Convert.ToInt32(issueResponse.issue_multiplier);
                                }

                                issueList.customDate1 = issueResponse.custom_date1;
                                if (issueResponse.date_added != null)
                                {
                                    issueList.dateAdded = formatDateTime(issueResponse.date_added);
                                }


                                issueList.addedByUserCode = issueResponse.added_by_user_code;

                                if (issueResponse.date_changed != null)
                                {
                                    issueList.dateChanged = formatDateTime(issueResponse.date_changed);
                                }


                                issueList.changedByUserCode = issueResponse.changed_by_user_code;
                                issueList.c05Code = issueResponse.c05_code;
                                issueList.c06Code = issueResponse.c06_code;
                                issueList.c07Code = issueResponse.c07_code;
                                issueList.c08Code = issueResponse.c08_code;
                                issueList.c09Code = issueResponse.c09_code;
                                issueList.c10Code = issueResponse.c10_code;
                                issueList.c11Code = issueResponse.c11_code;
                                issueList.c12Code = issueResponse.c12_code;
                                issueList.c13Code = issueResponse.c13_code;
                                issueList.c14Code = issueResponse.c14_code;
                                issueList.c15Code = issueResponse.c15_code;
                                issueList.c16Code = issueResponse.c16_code;
                                issueList.c17Code = issueResponse.c17_code;
                                if (issueResponse.c18_code != null)
                                {
                                    issueList.c18Code = formatDateTime(issueResponse.c18_code);
                                }

                                issueList.c19Code = issueResponse.c19_code;
                                issueList.c20Code = issueResponse.c20_code;
                                issueList.c21Code = issueResponse.c21_code;
                                issueList.c22Code = issueResponse.c22_code;
                                issueList.c23Code = issueResponse.c23_code;
                                issueList.c24Code = issueResponse.c24_code;
                                issueList.c25Code = issueResponse.c25_code;
                                if (issueResponse.c26_code != null)
                                {
                                    issueList.c26Code = formatDateTime(issueResponse.c26_code);
                                }

                                issueList.c27Code = issueResponse.c27_code;
                                issueList.c28Code = issueResponse.c28_code;
                                issueList.c29Code = issueResponse.c29_code;
                                issueList.c30Code = issueResponse.c30_code;
                                issueList.c31Code = issueResponse.c31_code;
                                issueList.c32Code = issueResponse.c32_code;
                                issueList.c33Code = issueResponse.c33_code;
                                issueList.c34Code = issueResponse.c34_code;
                                issueList.c35Code = issueResponse.c35_code;
                                issueList.c36Code = issueResponse.c36_code;
                                issueList.c37Code = issueResponse.c37_code;
                                issueList.c38Code = issueResponse.c38_code;
                                issueList.c39Code = issueResponse.c39_code;
                                issueList.c40Code = issueResponse.c40_code;
                                issueList.c41Code = issueResponse.c41_code;
                                issueList.c42Code = issueResponse.c42_code;
                                issueList.c43Code = issueResponse.c43_code;
                                issueList.c44Code = issueResponse.c44_code;
                                issueList.c45Code = issueResponse.c45_code;
                                issueList.c46Code = issueResponse.c46_code;
                                issueList.c47Code = issueResponse.c47_code;
                                issueList.c48Code = issueResponse.c48_code;
                                issueList.c49Code = issueResponse.c49_code;
                                issueList.c50Code = issueResponse.c50_code;
                                issueList.c51Code = issueResponse.c51_code;
                                issueList.c52Code = issueResponse.c52_code;
                                issueList.c53Code = issueResponse.c53_code;
                                issueList.c54Code = issueResponse.c54_code;
                                issueList.c55Code = issueResponse.c55_code;
                                issueList.c56Code = issueResponse.c56_code;
                                issueList.c57Code = issueResponse.c57_code;
                                issueList.c58Code = issueResponse.c58_code;
                                issueList.c59Code = issueResponse.c59_code;
                                issueList.c60Code = issueResponse.c60_code;
                                issueList.c61Code = issueResponse.c61_code;
                                issueList.c62Code = issueResponse.c62_code;
                                issueList.c63Code = issueResponse.c63_code;
                                issueList.c64Code = issueResponse.c64_code;
                                issueList.c65Code = issueResponse.c65_code;
                                issueList.c66Code = issueResponse.c66_code;
                                issueList.c67Code = issueResponse.c67_code;
                                issueList.c68Code = issueResponse.c68_code;
                                issueList.c69Code = issueResponse.c69_code;
                                issueList.c70Code = issueResponse.c70_code;
                                issueList.c71Code = issueResponse.c71_code;
                                issueList.c72Code = issueResponse.c72_code;
                                issueList.c73Code = issueResponse.c73_code;
                                issueList.c74Code = issueResponse.c74_code;
                                issueList.c75Code = issueResponse.c75_code;
                                issueList.c76Code = issueResponse.c76_code;
                                issueList.c77Code = issueResponse.c77_code;
                                issueList.c78Code = issueResponse.c78_code;
                                issueList.c79Code = issueResponse.c79_code;
                                issueList.c80Code = issueResponse.c80_code;
                                issueList.c81Code = issueResponse.c81_code;
                                issueList.c82Code = issueResponse.c82_code;
                                issueList.c83Code = issueResponse.c83_code;
                                issueList.c84Code = issueResponse.c84_code;
                                issueList.c85Code = issueResponse.c85_code;
                                issueList.c86Code = issueResponse.c86_code;
                                issueList.c87Code = issueResponse.c87_code;

                                if (issueResponse.c88_code != null)
                                {
                                    //  issueList.c88Code = formatDateTime(issueResponse.c88_code);
                                    issueList.c88Code = issueResponse.c88_code;
                                }

                                issueList.c89Code = issueResponse.c89_code;
                                issueList.c90Code = issueResponse.c90_code;
                                issueList.c91Code = issueResponse.c91_code;
                                issueList.c92Code = issueResponse.c92_code;
                                issueList.c93Code = issueResponse.c93_code;
                                issueList.c94Code = issueResponse.c94_code;
                                issueList.c95Code = issueResponse.c95_code;
                                issueList.c96Code = issueResponse.c96_code;
                                issueList.c97Code = issueResponse.c97_code;
                                issueList.c98Code = issueResponse.c98_code;
                                issueList.c99Code = issueResponse.c99_code;


                                response.issueList.Add(issueList);
                            }

                        }
                    }



                    if (responseCase.caseTextList != null)
                    {



                        if (responseCase.caseTextList.caseText != null)
                            PrepareCaseTextForResponse(responseCase, response);

                    }

                    if (responseCase.addressList != null)
                    {

                        if (responseCase.addressList.Address != null)
                        {
                            response.addressList = new List<AddressList>();
                            foreach (Model.Epc.Address1 addressResponse in responseCase.addressList.Address)
                            {
                                AddressList addressList = new AddressList
                                {
                                    addressId = Convert.ToInt32(addressResponse.address_id),
                                    addressTypeCode = addressResponse.address_type_code,
                                    addressCode = addressResponse.address_code,
                                    active = (addressResponse.active == "Y"),
                                    accountNumber = addressResponse.account_number,
                                    nameTitle = addressResponse.name_title,
                                    givenNames = addressResponse.given_names,
                                    middleInitial = addressResponse.middle_initial != null ? Convert.ToChar(addressResponse.middle_initial) : '\0',
                                    lastName = addressResponse.last_name,
                                    suffix = addressResponse.suffix,
                                    companyName = addressResponse.company_name,
                                    jobTitle = addressResponse.job_title,
                                    address1 = addressResponse.address1,
                                    address2 = addressResponse.address2,
                                    address3 = addressResponse.address3,
                                    city = addressResponse.city,
                                    state = addressResponse.state,
                                    postalCode = addressResponse.postal_code,
                                    country = addressResponse.country,
                                    email = addressResponse.email,
                                    email2 = addressResponse.email2,
                                    email3 = addressResponse.email3,
                                    searchName = addressResponse.search_name,
                                    searchAddress = addressResponse.search_address,
                                    originatedVia = addressResponse.originated_via,



                                    allowSurvey = (addressResponse.allow_survey == "Y"),


                                    whereToBuy = addressResponse.where_to_buy,

                                    instructions = addressResponse.instructions,
                                    repeaterCode = addressResponse.repeater_code,

                                    county = addressResponse.county,
                                    optOut = (addressResponse.opt_out == "Y"),
                                    languageId = addressResponse.language_id,
                                    primaryAddressId = addressResponse.primary_address_id,
                                    currentCode = addressResponse.current_code,

                                    addedByUserCode = addressResponse.added_by_user_code,

                                    changedByUserCode = addressResponse.changed_by_user_code,
                                    a05Code = addressResponse.a05_code,
                                    a06Code = addressResponse.a06_code,
                                    a07Code = addressResponse.a07_code,
                                    a08Code = addressResponse.a08_code,
                                    a09Code = addressResponse.a09_code,
                                    a10Code = addressResponse.a10_code,
                                    a11Code = addressResponse.a11_code,
                                    a12Code = addressResponse.a12_code,
                                    a13Code = addressResponse.a13_code,
                                    a14Code = addressResponse.a14_code,
                                    a15Code = addressResponse.a15_code,
                                    a16Code = addressResponse.a16_code,
                                    a17Code = addressResponse.a17_code,
                                    a18Code = addressResponse.a18_code,
                                    a19Code = addressResponse.a19_code,
                                    a20Code = addressResponse.a20_code,
                                    a21Code = addressResponse.a21_code,
                                    a22Code = addressResponse.a22_code,
                                    a23Code = addressResponse.a24_code,
                                    a25Code = addressResponse.a25_code,
                                    a26Code = addressResponse.a26_code,
                                    a27Code = addressResponse.a27_code,
                                    a28Code = addressResponse.a28_code,
                                    a29Code = addressResponse.a29_code,
                                    a30Code = addressResponse.a30_code,
                                    a31Code = addressResponse.a31_code,
                                    a32Code = addressResponse.a32_code,
                                    a33Code = addressResponse.a33_code,
                                    a34Code = addressResponse.a34_code,
                                    a35Code = addressResponse.a35_code,
                                    a36Code = addressResponse.a36_code,
                                    a37Code = addressResponse.a37_code,
                                    a38Code = addressResponse.a38_code,
                                    a39Code = addressResponse.a39_code,
                                    a40Code = addressResponse.a40_code,
                                    a41Code = addressResponse.a41_code,
                                    a42Code = addressResponse.a42_code,
                                    a43Code = addressResponse.a43_code,
                                    a44Code = addressResponse.a44_code,
                                    a45Code = addressResponse.a45_code,
                                    a46Code = addressResponse.a46_code,
                                    a47Code = addressResponse.a47_code,
                                    a48Code = addressResponse.a48_code,
                                    a49Code = addressResponse.a49_code,
                                    a50Code = addressResponse.a50_code,
                                    a51Code = addressResponse.a51_code,
                                    a52Code = addressResponse.a52_code,
                                    a53Code = addressResponse.a53_code,
                                    a54Code = addressResponse.a54_code,
                                    a55Code = addressResponse.a55_code,
                                    a56Code = addressResponse.a56_code,
                                    a57Code = addressResponse.a57_code,
                                    a58Code = addressResponse.a58_code,
                                    a59Code = addressResponse.a59_code,
                                    a60Code = addressResponse.a60_code,
                                    a61Code = addressResponse.a61_code,
                                    a62Code = addressResponse.a62_code,
                                    a63Code = addressResponse.a63_code,
                                    a64Code = addressResponse.a64_code,
                                    a65Code = addressResponse.a65_code,
                                    a66Code = addressResponse.a66_code,
                                    a67Code = addressResponse.a67_code,
                                    a68Code = addressResponse.a68_code,
                                    a69Code = addressResponse.a69_code,
                                    a70Code = addressResponse.a70_code,
                                    a71Code = addressResponse.a71_code,
                                    a72Code = addressResponse.a72_code,
                                    a73Code = addressResponse.a73_code,
                                    a74Code = addressResponse.a74_code,
                                    a75Code = addressResponse.a75_code,
                                    a76Code = addressResponse.a76_code,
                                    a77Code = addressResponse.a77_code,
                                    a78Code = addressResponse.a78_code,
                                    a79Code = addressResponse.a79_code,
                                    a80Code = addressResponse.a80_code
                                };
                                if (addressResponse.date_changed != null)
                                {
                                    addressList.dateChanged = formatDateTime(addressResponse.date_changed);
                                }
                                if (addressResponse.date_added != null)
                                {
                                    addressList.dateAdded = formatDateTime(addressResponse.date_added);
                                }
                                if (addressResponse.last_survey != null)
                                {
                                    addressList.lastSurvey = formatDateTime(addressResponse.last_survey);
                                }
                                if (addressResponse.last_contact != null)
                                {
                                    addressList.lastContact = formatDateTime(addressResponse.last_contact);
                                }
                                if (addressResponse.last_modified != null)
                                {
                                    addressList.lastModified = formatDateTime(addressResponse.last_modified);
                                }


                                if (addressResponse.originated_date != null)
                                {
                                    addressList.originatedDate = formatDateTime(addressResponse.originated_date);
                                }

                                if (addressResponse.accumulated_goodwill != null)
                                {
                                    addressList.accumulatedGoodwill = Convert.ToInt32(addressResponse.accumulated_goodwill);
                                }
                                if (addressResponse.encl_auth_level != null)
                                {
                                    addressList.enclAuthLevel = Convert.ToInt32(addressResponse.encl_auth_level);
                                }

                                if (addressResponse.latitude != null)
                                {
                                    addressList.latitude = Convert.ToDouble(addressResponse.latitude);
                                }

                                if (addressResponse.longitude != null)
                                {
                                    addressList.longitude = Convert.ToDouble(addressResponse.longitude);
                                }

                                response.addressList.Add(addressList);


                            }
                        }
                    }
                    return response;
                }



                return epcErrorResponse;

            }
            catch (Exception ex)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = ex.Message;
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;
                return epcErrorResponse;
            }
        }

        public object FormatCaseResponse(EpcCreateCase responsePayload)
        {
            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();
            try
            {
                if (responsePayload != null)
                {

                    if (responsePayload.Case != null)
                    {
                        Model.Epc.Case1 responseCase = responsePayload.Case[0];

                        CreateCaseRequest response = new CreateCaseRequest
                        {
                            companyId = responseCase.company_id,
                            caseId = Convert.ToInt32(responseCase.case_id),
                            initialUserCode = responseCase.initial_user_code,
                            responsibleUserCode = responseCase.responsible_user_code,
                            addressId = Convert.ToInt32(responseCase.address_id)
                        };
                        // response.caseStatus = "O" as =enum when responseCase.case_status == 'O' or responseCase.case_status == 'Open' otherwise "C" as =enum default null;
                        if (responseCase.case_status == "O" || responseCase.case_status == "Open")
                        {
                            response.caseStatus = "O";
                        }
                        else
                        {
                            response.caseStatus = "C";
                        }//need to recheck enum data type

                        response.caseStatusCode = responseCase.case_status_code;

                        if (responseCase.received != null)
                        {

                            response.received = formatDateTime(responseCase.received);
                        }

                        if (responseCase.closed != null)
                        {
                            response.closed = formatDateTime(responseCase.closed);

                        }


                        if (responseCase.total_work_time != null)
                        {

                            response.totalWorkTime = Convert.ToInt32(responseCase.total_work_time);
                        }

                        if (responseCase.contact_multiplier != null)
                        {
                            response.contactMultiplier = Convert.ToInt32(responseCase.contact_multiplier);

                        }

                        response.incomingPhone = responseCase.incoming_phone;
                        response.firstPickDate = responseCase.first_pick_date;
                        response.firstPickQueueUserCode = responseCase.first_pick_queue_user_code;
                        response.firstPickUserCode = responseCase.first_pick_user_code;

                        if (responseCase.first_pick_time != null)
                        {

                            response.firstPickTime = Convert.ToInt32(responseCase.first_pick_time);
                        }

                        //  response.queueTime = Convert.ToInt32(responseCase.queue_time);
                        if (responseCase.queue_time != null)
                        {
                            response.queueTime = Convert.ToInt32(responseCase.queue_time);
                        }

                        if (responseCase.queue_count != null)
                        {
                            response.queueCount = Convert.ToInt32(responseCase.queue_count);
                        }

                        if (responseCase.transfer_count != null)
                        {
                            response.transferCount = Convert.ToInt32(responseCase.transfer_count);
                        }


                        response.firstResponseDate = responseCase.first_response_date;

                        if (responseCase.first_response_time != null)
                        {
                            response.firstResponseTime = Convert.ToInt32(responseCase.first_response_time);
                        }

                        response.firstResponseUserCode = responseCase.first_response_user_code;
                        if (responseCase.communication_count != null)
                        {
                            response.communicationCount = Convert.ToInt32(responseCase.communication_count);
                        }

                        response.closedUserCode = responseCase.closed_user_code;

                        if (responseCase.closed_time != null)
                        {
                            response.closedTime = Convert.ToInt32(responseCase.closed_time);
                        }

                        response.pciStatus = responseCase.pci_status;
                        response.caseAddressRole = responseCase.case_address_role;

                        if (responseCase.date_added != null)
                        {
                            response.dateAdded = formatDateTime(responseCase.date_added);
                        }

                        response.addedByUserCode = responseCase.added_by_user_code;

                        if (responseCase.date_changed != null)
                        {
                            response.dateChanged = formatDateTime(responseCase.date_changed);
                        }


                        response.changedByUserCode = responseCase.changed_by_user_code;
                        response.agentAssist = responseCase.agent_assist;

                        if (responseCase.identity_id != null)
                        {
                            response.identityId = Convert.ToInt32(responseCase.identity_id);
                        }
                        response.b05Code = responseCase.b05_code;
                        response.b06Code = responseCase.b06_code;
                        response.b07Code = responseCase.b07_code;
                        response.b08Code = responseCase.b08_code;
                        response.b09Code = responseCase.b09_code;
                        response.b10Code = responseCase.b10_code;
                        response.b11Code = responseCase.b11_code;
                        response.b12Code = responseCase.b12_code;
                        response.b13Code = responseCase.b13_code;
                        response.b14Code = responseCase.b14_code;
                        response.b15Code = responseCase.b15_code;
                        response.b16Code = responseCase.b16_code;
                        response.b17Code = responseCase.b17_code;
                        response.b18Code = responseCase.b18_code;
                        response.b19Code = responseCase.b19_code;
                        response.b20Code = responseCase.b20_code;
                        response.b21Code = responseCase.b21_code;
                        response.b22Code = responseCase.b22_code;
                        response.b23Code = responseCase.b23_code;
                        response.b24Code = responseCase.b24_code;
                        response.b25Code = responseCase.b25_code;
                        response.b26Code = responseCase.b26_code;
                        response.b27Code = responseCase.b27_code;
                        response.b28Code = responseCase.b28_code;
                        response.b29Code = responseCase.b29_code;
                        response.b30Code = responseCase.b30_code;
                        response.b31Code = responseCase.b31_code;
                        response.b32Code = responseCase.b32_code;
                        response.b33Code = responseCase.b33_code;
                        response.b34Code = responseCase.b34_code;
                        response.b35Code = responseCase.b35_code;
                        response.b36Code = responseCase.b36_code;
                        response.b37Code = responseCase.b37_code;
                        response.b38Code = responseCase.b38_code;
                        response.b39Code = responseCase.b39_code;
                        response.b40Code = responseCase.b40_code;
                        response.b41Code = responseCase.b41_code;
                        response.b42Code = responseCase.b42_code;
                        response.b43Code = responseCase.b43_code;
                        response.b44Code = responseCase.b44_code;
                        response.b45Code = responseCase.b45_code;
                        response.b46Code = responseCase.b46_code;
                        response.b47Code = responseCase.b47_code;
                        response.b48Code = responseCase.b48_code;
                        response.b49Code = responseCase.b49_code;

                        if (responseCase.IssueList != null)
                        {
                            if (responseCase.IssueList.Issue != null)
                            {
                                response.issueList = new List<IssueList>();
                                foreach (Model.Epc.Issue1 issueResponse in responseCase.IssueList.Issue)
                                {
                                    IssueList issueList = new IssueList
                                    {
                                        issueSeq = Convert.ToInt32(issueResponse.issue_seq),
                                        productCode = issueResponse.product_code,
                                        assignedToUserCode = issueResponse.assigned_to_user_code
                                    };
                                    //issueList.issueStatus = "O" as =enum when issueResponse.issue_status == 'O' or issueResponse.issue_status == 'Open' otherwise "C" as =enum default null;
                                    if (issueResponse.issue_status == "O" || issueResponse.issue_status == "Open")
                                    {
                                        issueList.issueStatus = "O";
                                    }
                                    else
                                    {
                                        issueList.issueStatus = "C";
                                    }

                                    issueList.issueStatusCode = issueResponse.issue_status_code;
                                    if (issueResponse.closed != null)
                                    {
                                        issueList.closed = formatDateTime(issueResponse.closed);

                                    }


                                    if (issueResponse.issue_multiplier != null)
                                    {
                                        issueList.issueMultiplier = Convert.ToInt32(issueResponse.issue_multiplier);
                                    }

                                    issueList.customDate1 = issueResponse.custom_date1;
                                    if (issueResponse.date_added != null)
                                    {
                                        issueList.dateAdded = formatDateTime(issueResponse.date_added);
                                    }


                                    issueList.addedByUserCode = issueResponse.added_by_user_code;

                                    if (issueResponse.date_changed != null)
                                    {
                                        issueList.dateChanged = formatDateTime(issueResponse.date_changed);
                                    }


                                    issueList.changedByUserCode = issueResponse.changed_by_user_code;
                                    issueList.c05Code = issueResponse.c05_code;
                                    issueList.c06Code = issueResponse.c06_code;
                                    issueList.c07Code = issueResponse.c07_code;
                                    issueList.c08Code = issueResponse.c08_code;
                                    issueList.c09Code = issueResponse.c09_code;
                                    issueList.c10Code = issueResponse.c10_code;
                                    issueList.c11Code = issueResponse.c11_code;
                                    issueList.c12Code = issueResponse.c12_code;
                                    issueList.c13Code = issueResponse.c13_code;
                                    issueList.c14Code = issueResponse.c14_code;
                                    issueList.c15Code = issueResponse.c15_code;
                                    issueList.c16Code = issueResponse.c16_code;
                                    issueList.c17Code = issueResponse.c17_code;
                                    if (issueResponse.c18_code != null)
                                    {
                                        issueList.c18Code = formatDateTime(issueResponse.c18_code);
                                    }

                                    issueList.c19Code = issueResponse.c19_code;
                                    issueList.c20Code = issueResponse.c20_code;
                                    issueList.c21Code = issueResponse.c21_code;
                                    issueList.c22Code = issueResponse.c22_code;
                                    issueList.c23Code = issueResponse.c23_code;
                                    issueList.c24Code = issueResponse.c24_code;
                                    issueList.c25Code = issueResponse.c25_code;
                                    if (issueResponse.c26_code != null)
                                    {
                                        issueList.c26Code = formatDateTime(issueResponse.c26_code);
                                    }

                                    issueList.c27Code = issueResponse.c27_code;
                                    issueList.c28Code = issueResponse.c28_code;
                                    issueList.c29Code = issueResponse.c29_code;
                                    issueList.c30Code = issueResponse.c30_code;
                                    issueList.c31Code = issueResponse.c31_code;
                                    issueList.c32Code = issueResponse.c32_code;
                                    issueList.c33Code = issueResponse.c33_code;
                                    issueList.c34Code = issueResponse.c34_code;
                                    issueList.c35Code = issueResponse.c35_code;
                                    issueList.c36Code = issueResponse.c36_code;
                                    issueList.c37Code = issueResponse.c37_code;
                                    issueList.c38Code = issueResponse.c38_code;
                                    issueList.c39Code = issueResponse.c39_code;
                                    issueList.c40Code = issueResponse.c40_code;
                                    issueList.c41Code = issueResponse.c41_code;
                                    issueList.c42Code = issueResponse.c42_code;
                                    issueList.c43Code = issueResponse.c43_code;
                                    issueList.c44Code = issueResponse.c44_code;
                                    issueList.c45Code = issueResponse.c45_code;
                                    issueList.c46Code = issueResponse.c46_code;
                                    issueList.c47Code = issueResponse.c47_code;
                                    issueList.c48Code = issueResponse.c48_code;
                                    issueList.c49Code = issueResponse.c49_code;
                                    issueList.c50Code = issueResponse.c50_code;
                                    issueList.c51Code = issueResponse.c51_code;
                                    issueList.c52Code = issueResponse.c52_code;
                                    issueList.c53Code = issueResponse.c53_code;
                                    issueList.c54Code = issueResponse.c54_code;
                                    issueList.c55Code = issueResponse.c55_code;
                                    issueList.c56Code = issueResponse.c56_code;
                                    issueList.c57Code = issueResponse.c57_code;
                                    issueList.c58Code = issueResponse.c58_code;
                                    issueList.c59Code = issueResponse.c59_code;
                                    issueList.c60Code = issueResponse.c60_code;
                                    issueList.c61Code = issueResponse.c61_code;
                                    issueList.c62Code = issueResponse.c62_code;
                                    issueList.c63Code = issueResponse.c63_code;
                                    issueList.c64Code = issueResponse.c64_code;
                                    issueList.c65Code = issueResponse.c65_code;
                                    issueList.c66Code = issueResponse.c66_code;
                                    issueList.c67Code = issueResponse.c67_code;
                                    issueList.c68Code = issueResponse.c68_code;
                                    issueList.c69Code = issueResponse.c69_code;
                                    issueList.c70Code = issueResponse.c70_code;
                                    issueList.c71Code = issueResponse.c71_code;
                                    issueList.c72Code = issueResponse.c72_code;
                                    issueList.c73Code = issueResponse.c73_code;
                                    issueList.c74Code = issueResponse.c74_code;
                                    issueList.c75Code = issueResponse.c75_code;
                                    issueList.c76Code = issueResponse.c76_code;
                                    issueList.c77Code = issueResponse.c77_code;
                                    issueList.c78Code = issueResponse.c78_code;
                                    issueList.c79Code = issueResponse.c79_code;
                                    issueList.c80Code = issueResponse.c80_code;
                                    issueList.c81Code = issueResponse.c81_code;
                                    issueList.c82Code = issueResponse.c82_code;


                                    //if (issueList.c83Code != null && responseCase.company_id?.ToUpper() != "JPM")
                                    //{
                                    //    issueList.c83Code = issueResponse.c83_code;
                                    //}
                                    if (issueResponse.c83_code != null)
                                    {
                                        issueList.c83Code = formatDateTime(issueResponse.c83_code);

                                    }

                                    issueList.c84Code = issueResponse.c84_code;
                                    issueList.c85Code = issueResponse.c85_code;
                                    issueList.c86Code = issueResponse.c86_code;
                                    issueList.c87Code = issueResponse.c87_code;

                                    //if (issueResponse.c88_code != null && responseCase.company_id?.ToUpper() != "JPM")
                                    //{
                                    //    issueList.c88Code = issueResponse.c88_code;
                                    //}

                                    if (issueResponse.c88_code != null)
                                    {
                                        issueList.c88Code = formatDateTime(issueResponse.c88_code);

                                    }

                                    issueList.c89Code = issueResponse.c89_code;
                                    issueList.c90Code = issueResponse.c90_code;
                                    issueList.c91Code = issueResponse.c91_code;
                                    issueList.c92Code = issueResponse.c92_code;
                                    issueList.c93Code = issueResponse.c93_code;
                                    issueList.c94Code = issueResponse.c94_code;
                                    issueList.c95Code = issueResponse.c95_code;
                                    issueList.c96Code = issueResponse.c96_code;
                                    issueList.c97Code = issueResponse.c97_code;
                                    issueList.c98Code = issueResponse.c98_code;
                                    issueList.c99Code = issueResponse.c99_code;


                                    response.issueList.Add(issueList);
                                }

                            }
                        }



                        if (responseCase.caseTextList != null)
                        {



                            if (responseCase.caseTextList.caseText != null)
                                PrepareCaseTextForResponse(responseCase, response);

                        }

                        if (responseCase.addressList != null)
                        {

                            if (responseCase.addressList.Address != null)
                            {
                                response.addressList = new List<AddressList>();
                                foreach (Model.Epc.Address1 addressResponse in responseCase.addressList.Address)
                                {
                                    AddressList addressList = new AddressList
                                    {
                                        addressId = Convert.ToInt32(addressResponse.address_id),
                                        addressTypeCode = addressResponse.address_type_code,
                                        addressCode = addressResponse.address_code,
                                        active = (addressResponse.active == "Y"),
                                        accountNumber = addressResponse.account_number,
                                        nameTitle = addressResponse.name_title,
                                        givenNames = addressResponse.given_names,
                                        middleInitial = addressResponse.middle_initial != null ? Convert.ToChar(addressResponse.middle_initial) : '\0',
                                        lastName = addressResponse.last_name,
                                        suffix = addressResponse.suffix,
                                        companyName = addressResponse.company_name,
                                        jobTitle = addressResponse.job_title,
                                        address1 = addressResponse.address1,
                                        address2 = addressResponse.address2,
                                        address3 = addressResponse.address3,
                                        city = addressResponse.city,
                                        state = addressResponse.state,
                                        postalCode = addressResponse.postal_code,
                                        country = addressResponse.country,
                                        email = addressResponse.email,
                                        email2 = addressResponse.email2,
                                        email3 = addressResponse.email3,
                                        searchName = addressResponse.search_name,
                                        searchAddress = addressResponse.search_address,
                                        originatedVia = addressResponse.originated_via,



                                        allowSurvey = (addressResponse.allow_survey == "Y"),


                                        whereToBuy = addressResponse.where_to_buy,

                                        instructions = addressResponse.instructions,
                                        repeaterCode = addressResponse.repeater_code,

                                        county = addressResponse.county,
                                        optOut = (addressResponse.opt_out == "Y"),
                                        languageId = addressResponse.language_id,
                                        primaryAddressId = addressResponse.primary_address_id,
                                        currentCode = addressResponse.current_code,

                                        addedByUserCode = addressResponse.added_by_user_code,

                                        changedByUserCode = addressResponse.changed_by_user_code,
                                        a05Code = addressResponse.a05_code,
                                        a06Code = addressResponse.a06_code,
                                        a07Code = addressResponse.a07_code,
                                        a08Code = addressResponse.a08_code,
                                        a09Code = addressResponse.a09_code,
                                        a10Code = addressResponse.a10_code,
                                        a11Code = addressResponse.a11_code,
                                        a12Code = addressResponse.a12_code,
                                        a13Code = addressResponse.a13_code,
                                        a14Code = addressResponse.a14_code,
                                        a15Code = addressResponse.a15_code,
                                        a16Code = addressResponse.a16_code,
                                        a17Code = addressResponse.a17_code,
                                        a18Code = addressResponse.a18_code,
                                        a19Code = addressResponse.a19_code,
                                        a20Code = addressResponse.a20_code,
                                        a21Code = addressResponse.a21_code,
                                        a22Code = addressResponse.a22_code,
                                        a23Code = addressResponse.a24_code,
                                        a25Code = addressResponse.a25_code,
                                        a26Code = addressResponse.a26_code,
                                        a27Code = addressResponse.a27_code,
                                        a28Code = addressResponse.a28_code,
                                        a29Code = addressResponse.a29_code,
                                        a30Code = addressResponse.a30_code,
                                        a31Code = addressResponse.a31_code,
                                        a32Code = addressResponse.a32_code,
                                        a33Code = addressResponse.a33_code,
                                        a34Code = addressResponse.a34_code,
                                        a35Code = addressResponse.a35_code,
                                        a36Code = addressResponse.a36_code,
                                        a37Code = addressResponse.a37_code,
                                        a38Code = addressResponse.a38_code,
                                        a39Code = addressResponse.a39_code,
                                        a40Code = addressResponse.a40_code,
                                        a41Code = addressResponse.a41_code,
                                        a42Code = addressResponse.a42_code,
                                        a43Code = addressResponse.a43_code,
                                        a44Code = addressResponse.a44_code,
                                        a45Code = addressResponse.a45_code,
                                        a46Code = addressResponse.a46_code,
                                        a47Code = addressResponse.a47_code,
                                        a48Code = addressResponse.a48_code,
                                        a49Code = addressResponse.a49_code,
                                        a50Code = addressResponse.a50_code,
                                        a51Code = addressResponse.a51_code,
                                        a52Code = addressResponse.a52_code,
                                        a53Code = addressResponse.a53_code,
                                        a54Code = addressResponse.a54_code,
                                        a55Code = addressResponse.a55_code,
                                        a56Code = addressResponse.a56_code,
                                        a57Code = addressResponse.a57_code,
                                        a58Code = addressResponse.a58_code,
                                        a59Code = addressResponse.a59_code,
                                        a60Code = addressResponse.a60_code,
                                        a61Code = addressResponse.a61_code,
                                        a62Code = addressResponse.a62_code,
                                        a63Code = addressResponse.a63_code,
                                        a64Code = addressResponse.a64_code,
                                        a65Code = addressResponse.a65_code,
                                        a66Code = addressResponse.a66_code,
                                        a67Code = addressResponse.a67_code,
                                        a68Code = addressResponse.a68_code,
                                        a69Code = addressResponse.a69_code,
                                        a70Code = addressResponse.a70_code,
                                        a71Code = addressResponse.a71_code,
                                        a72Code = addressResponse.a72_code,
                                        a73Code = addressResponse.a73_code,
                                        a74Code = addressResponse.a74_code,
                                        a75Code = addressResponse.a75_code,
                                        a76Code = addressResponse.a76_code,
                                        a77Code = addressResponse.a77_code,
                                        a78Code = addressResponse.a78_code,
                                        a79Code = addressResponse.a79_code,
                                        a80Code = addressResponse.a80_code
                                    };
                                    if (addressResponse.date_changed != null)
                                    {
                                        addressList.dateChanged = formatDateTime(addressResponse.date_changed);
                                    }
                                    if (addressResponse.date_added != null)
                                    {
                                        addressList.dateAdded = formatDateTime(addressResponse.date_added);
                                    }
                                    if (addressResponse.last_survey != null)
                                    {
                                        addressList.lastSurvey = formatDateTime(addressResponse.last_survey);
                                    }
                                    if (addressResponse.last_contact != null)
                                    {
                                        addressList.lastContact = formatDateTime(addressResponse.last_contact);
                                    }
                                    if (addressResponse.last_modified != null)
                                    {
                                        addressList.lastModified = formatDateTime(addressResponse.last_modified);
                                    }


                                    if (addressResponse.originated_date != null)
                                    {
                                        addressList.originatedDate = formatDateTime(addressResponse.originated_date);
                                    }

                                    if (addressResponse.accumulated_goodwill != null)
                                    {
                                        addressList.accumulatedGoodwill = Convert.ToInt32(addressResponse.accumulated_goodwill);
                                    }
                                    if (addressResponse.encl_auth_level != null)
                                    {
                                        addressList.enclAuthLevel = Convert.ToInt32(addressResponse.encl_auth_level);
                                    }

                                    if (addressResponse.latitude != null)
                                    {
                                        addressList.latitude = Convert.ToDouble(addressResponse.latitude);
                                    }

                                    if (addressResponse.longitude != null)
                                    {
                                        addressList.longitude = Convert.ToDouble(addressResponse.longitude);
                                    }

                                    response.addressList.Add(addressList);


                                }
                            }
                        }
                        _log.LogDebug("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: FormatCaseResponse " + " Message:  " + response.ToString());
                        return response;
                    }
                }


                return epcErrorResponse;

            }
            catch (Exception ex)
            {
                epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("epc_INTERNALERROR_000");
                epcErrorResponse.message = ex.Message;
                epcErrorResponse.httpstatuscode = HttpStatusCode.InternalServerError;
                _log.LogError("###### Correlation ID : " + _headerDict["X-Correlation-Id"].ToString() + "|| Method Name: FormatCaseResponse " + " Error Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);
                return epcErrorResponse;
            }
        }

        private static void PrepareCaseTextForResponse(Model.Epc.Case1 responseCase, CreateCaseRequest response)
        {
            response.caseTextList = new Model.CaseTextList1();
            response.caseTextList.caseText = new Model.CaseText1[responseCase.caseTextList.caseText.Length];
            foreach (CaseText1 caseTextResponse in responseCase.caseTextList.caseText)
            {
                Aspire.ePC.Model.CaseText1 caseText = new Aspire.ePC.Model.CaseText1
                {
                    case_text = caseTextResponse.case_text,
                    description = caseTextResponse.description,
                    text_type_code = caseTextResponse.text_type_code,
                    modified_by_user_code = caseTextResponse.modified_by_user_code,
                    added_by_user_code = caseTextResponse.added_by_user_code,
                    last_modified = caseTextResponse.last_modified,
                    date_added = caseTextResponse.date_added,
                    identity_id = caseTextResponse.identity_id,
                    case_text_seq_sort = caseTextResponse.case_text_seq_sort,
                    last_modified_sort = caseTextResponse.last_modified_sort,
                    issue_seq_sort = caseTextResponse.issue_seq_sort,
                    date_added_sort = caseTextResponse.date_added_sort,
                    identity_id_sort = caseTextResponse.identity_id_sort
                };

                if (caseTextResponse.case_text_seq != null)
                {
                    caseText.case_text_seq = caseTextResponse.case_text_seq;
                }

                if (caseTextResponse.action_seq != null)
                {
                    caseText.action_seq = caseTextResponse.action_seq;
                }

                if (caseTextResponse.issue_seq != null)
                {
                    caseText.issue_seq = caseTextResponse.issue_seq;
                };


                response.caseTextList.caseText[0] = caseText;
            }
        }

    }
}
