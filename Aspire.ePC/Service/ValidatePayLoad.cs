﻿using CoreCommon.Contract;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Aspire.Dining.Reservation.Exp.Selfserve.Services
{
    internal class ValidatePayLoad : IValidatePayLoad
    {
        public readonly string _clientID;
        public readonly string _requestBody;
        public readonly string _accessToken;
        public readonly IOktaToken _oktaClient;
        public readonly IHttpClient _httpClient;



        public ValidatePayLoad(string clientID, string requestBody, string accessToken, IOktaToken oktaClient, IHttpClient httpClient)
        {
            _clientID = clientID;
            _requestBody = requestBody;
            _accessToken = accessToken;
            _oktaClient = oktaClient;
            _httpClient = httpClient;
        }

        public async Task<ValidateResponse> ValidateRequest(IDictionary<string, string> headerDict, ExecutionContext executionContext, string transactionType)
        {

            return await validate(_clientID, _requestBody, _accessToken, headerDict, executionContext, transactionType);

        }

        public async Task<ValidateResponse> ValidateOKTA(JObject requestData, IDictionary<string, string> headerDict)
        {
            ValidateResponse response = new ValidateResponse();
            try
            {
                string[] DefaultClientsList = _configurationServices.DefaultValidationClients.Split(",");                
                string OktaURL = string.Empty;
                if(DefaultClientsList.Contains(headerDict["X-Organization"].ToUpper()))
                {
                    OktaURL = _configurationServices.DefaultIntrospectUrl;
                }
                else
                {
                    OktaURL = _configurationServices.IntrospectUrl;
                }

                if (!string.IsNullOrEmpty(_accessToken))
                {
                    string tokenResponse = await _oktaClient.PostAsync(_configurationServices.IntrospectClientId,
                        _configurationServices.IntrospectClientSecret,OktaURL, _accessToken, headerDict);

                    TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);

                    if (token.uid != null)
                    {
                        System.Net.Http.HttpResponseMessage oktaUserResponse = await _httpClient.GetAsync(_configurationServices.OKTA_UserProfile + "/" + token.uid, null,
                            _configurationServices.OKTA_ApiKey, _configurationServices.OKTA_keyType);

                        string responseContent = await oktaUserResponse.Content.ReadAsStringAsync();

                        OktaUser oktaUser = JsonConvert.DeserializeObject<OktaUser>(responseContent);

                        if (oktaUser.profile.organization.ToLower() == headerDict["X-Organization"].ToLower())
                        {
                            string sourceSystemOrcontactMethod_Value = string.Empty;
                            if (headerDict.ContainsKey("sourceSystem") && !string.IsNullOrEmpty(headerDict["sourceSystem"]))
                                sourceSystemOrcontactMethod_Value = headerDict["sourceSystem"];
                            else if (headerDict.ContainsKey("contactMethod") && !string.IsNullOrEmpty(headerDict["contactMethod"]))
                                sourceSystemOrcontactMethod_Value = headerDict["contactMethod"];

                            //oktaUser.profile.partyId
                            if (sourceSystemOrcontactMethod_Value.ToLower() == "concierge" && oktaUser.profile.userType.ToLower() == "user")
                            {
                                LogError(response, "FORBIDDEN_005", "FORBIDDEN_005", HttpStatusCode.Forbidden);
                                response.httpstatuscode = HttpStatusCode.Forbidden;
                                response.errorCode = "FORBIDDEN_005";
                                response.message = "Prohibited account type: User.";
                                return response;
                            }
                            else if (sourceSystemOrcontactMethod_Value.ToLower() == "self-serve" && oktaUser.profile.userType.ToLower() != "user")
                            {
                                LogError(response, "FORBIDDEN_003", "FORBIDDEN_003", HttpStatusCode.Forbidden);
                                response.httpstatuscode = HttpStatusCode.Forbidden;
                                response.errorCode = "FORBIDDEN_003";
                                response.message = "Prohibited account type: Service.";
                                return response;
                            }
                            else if (sourceSystemOrcontactMethod_Value.ToLower() == "chatbot" && oktaUser.profile.userType.ToLower() != "user")
                            {
                                LogError(response, "FORBIDDEN_003", "FORBIDDEN_003", HttpStatusCode.Forbidden);
                                response.httpstatuscode = HttpStatusCode.Forbidden;
                                response.errorCode = "FORBIDDEN_003";
                                response.message = "Prohibited account type: Service.";
                                return response;
                            }
                            if (sourceSystemOrcontactMethod_Value.ToLower() != "concierge" && oktaUser.profile.partyId != null)
                            {
                                response.partyId = oktaUser.profile.partyId;
                                // return response;
                            }
                            //else
                            //{
                            //    //LogError(response, "CONFIGERROR_000", "CONFIGERROR_000", HttpStatusCode.Forbidden);
                            //    //response.errorCode = "CONFIGERROR_000";
                            //    //response.message = "The server has an internal configuration error: Missing configuration setting. Please contact Aspire Administrator.";
                            //    //return response;

                            //}
                        }
                        else
                        {

                            LogError(response, "INVALIDORG_000", "INVALIDORG_000", HttpStatusCode.Unauthorized);
                            response.httpstatuscode = HttpStatusCode.BadRequest;
                            response.errorCode = "INVALIDORG_000";
                            response.message = "Organization is empty or Access token and Organization combination doesnt match.";
                            return response;
                        }
                    }
                    else
                    {
                        LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
                        response.httpstatuscode = HttpStatusCode.Unauthorized;
                        response.errorCode = "UNAUTHORIZED_001";
                        response.message = "Access token is either invalid or has expired.";
                        return response;
                    }
                }
                else
                {
                    LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Forbidden);
                    response.httpstatuscode = HttpStatusCode.Unauthorized;
                    response.errorCode = "FORBIDDEN_004";
                    response.message = "Authorization header is missing or not of type Bearer access_token.";
                    return response;
                }
            }
            catch (Exception ex)
            {
                LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
                response.httpstatuscode = HttpStatusCode.Unauthorized;
                response.errorCode = "UNAUTHORIZED_001";
                response.message = "Access token is either invalid or has expired.";
                return response;

            }
            return response;
        }

        public async Task<ValidateResponse> validate(string clientID, string requestBody, string accessToken, IDictionary<string, string> headerDict, ExecutionContext executionContext, string transactionType)
        {

            ValidateResponse response = new ValidateResponse();
            try
            {
                JObject requestData = JsonConvert.DeserializeObject<JObject>(requestBody);



                if (string.IsNullOrEmpty(_clientID))
                {

                    return LogError(response, Environment.GetEnvironmentVariable("INVALIDORG_000_Message"), Environment.GetEnvironmentVariable("INVALIDORG_000"), HttpStatusCode.BadRequest);
                }
                //if (string.IsNullOrEmpty(headerDict["X-Party-Id"]))
                //{
                //    return LogError(response, Environment.GetEnvironmentVariable("INVALIDParty_000_Message"), Environment.GetEnvironmentVariable("INVALIDParty_000"), HttpStatusCode.BadRequest);
                //}

                response = await ValidateOKTA(requestData, headerDict);
              
                if (response.message != null)
                {
                    return response;
                }
            }
            catch (Exception ex)
            {

                return LogError(response, ex.Message, Environment.GetEnvironmentVariable("INTERNALERROR_000"), HttpStatusCode.InternalServerError);
            }
            return response;

        }


        public async Task<ValidateResponse> ValidateSearchRequest(IDictionary<string, string> headers)
        {
            ValidateResponse response = new ValidateResponse();
            try
            {
                if (string.IsNullOrEmpty(_clientID))
                {
                    return LogError(response, Environment.GetEnvironmentVariable("INVALIDORG_000_Message"), Environment.GetEnvironmentVariable("INVALIDORG_000"), HttpStatusCode.BadRequest);
                }
                if (!string.IsNullOrEmpty(_accessToken))
                {
                    string tokenResponse = await _oktaClient.PostAsync(Environment.GetEnvironmentVariable("OKTA_ClientID"),
                                                                    Environment.GetEnvironmentVariable("OKTA_ClientSecret"),
                                                                    Environment.GetEnvironmentVariable("OKTA_validationURL"), _accessToken, headers);

                    TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);


                    if (token.uid != null)
                    {

                        System.Net.Http.HttpResponseMessage oktaUserResponse = await _httpClient.GetAsync(Environment.GetEnvironmentVariable("OKTA_UserProfile") + "/" + token.uid, null, Environment.GetEnvironmentVariable("OKTA_ApiKey"), Environment.GetEnvironmentVariable("OKTA_keyType"));
                        string responseContent = await oktaUserResponse.Content.ReadAsStringAsync();

                        OktaUser oktaUser = JsonConvert.DeserializeObject<OktaUser>(responseContent);

                        if (oktaUser.profile.organization.ToLower() == _clientID.ToLower())
                        {
                            //if (oktaUser.profile.partyId.ToLower() == headers["X-Party-Id"].ToLower())
                            //{
                            //    if (oktaUser.profile.userType.ToLower() == "service")
                            //    {
                            //        return LogError(response, Environment.GetEnvironmentVariable("FORBIDDEN_003_Message"), Environment.GetEnvironmentVariable("FORBIDDEN_003"), HttpStatusCode.Forbidden);

                            //    }
                            //}
                            //else
                            //{
                            //    return LogError(response, Environment.GetEnvironmentVariable("INVALIDParty_000_Message"), Environment.GetEnvironmentVariable("INVALIDParty_000"), HttpStatusCode.BadRequest);

                            //}

                        }
                        else
                        {

                            return LogError(response, Environment.GetEnvironmentVariable("INVALIDORG_000_Message"), Environment.GetEnvironmentVariable("INVALIDORG_000"), HttpStatusCode.BadRequest);
                        }
                    }
                    else
                    {
                        return LogError(response, Environment.GetEnvironmentVariable("UnAuthorised_Message_001"), Environment.GetEnvironmentVariable("UnAuthorised_001"), HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    return LogError(response, Environment.GetEnvironmentVariable("UnAuthorised_Message_001"), Environment.GetEnvironmentVariable("UnAuthorised_001"), HttpStatusCode.Unauthorized);

                }

            }
            catch (Exception ex)
            {
                return LogError(response, ex.Message, Environment.GetEnvironmentVariable("INTERNALERROR_000"), HttpStatusCode.InternalServerError);
            }
            return response;


        }

        public async Task<ValidateResponse> GetBookerPartyId(dynamic requestData, IDictionary<string, string> headerDict)
        {
            ValidateResponse response = new ValidateResponse();
            try
            {
                if (!string.IsNullOrEmpty(_accessToken))
                {
                    string tokenResponse = await _oktaClient.PostAsync(_configurationServices.IntrospectClientId,
                        _configurationServices.IntrospectClientSecret, _configurationServices.IntrospectUrl, _accessToken, headerDict);

                    TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);

                    if (token.uid != null)
                    {
                        System.Net.Http.HttpResponseMessage oktaUserResponse = await _httpClient.GetAsync(_configurationServices.OKTA_UserProfile + "/" + token.uid, null,
                            _configurationServices.OKTA_ApiKey, _configurationServices.OKTA_keyType);

                        string responseContent = await oktaUserResponse.Content.ReadAsStringAsync();

                        OktaUser oktaUser = JsonConvert.DeserializeObject<OktaUser>(responseContent);

                        if (oktaUser.profile.organization.ToLower() == headerDict["X-Organization"].ToLower())
                        {
                            //oktaUser.profile.partyId
                            if (headerDict["contactMethod"].ToString().ToLower() == "concierge" && oktaUser.profile.userType.ToLower() == "user")
                            {
                                LogError(response, "FORBIDDEN_003", "FORBIDDEN_003", HttpStatusCode.Forbidden);
                                response.httpstatuscode = HttpStatusCode.Forbidden;
                                response.errorCode = "FORBIDDEN_003";
                                response.message = "Prohibited account type: User.";
                                return response;
                            }
                            
                            if (oktaUser.profile.partyId != null)
                            {
                                response.partyId = oktaUser.profile.partyId;
                                return response;
                            }
                            //else
                            //{
                            //    //LogError(response, "CONFIGERROR_000", "CONFIGERROR_000", HttpStatusCode.Forbidden);
                            //    //response.errorCode = "CONFIGERROR_000";
                            //    //response.message = "The server has an internal configuration error: Missing configuration setting. Please contact Aspire Administrator.";
                            //    //return response;

                            //}
                        }
                        else
                        {

                            LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
                            response.httpstatuscode = HttpStatusCode.Unauthorized;
                            response.errorCode = "UNAUTHORIZED_001";
                            response.message = "Access token is either invalid or has expired.";
                            return response;
                        }
                    }
                    else
                    {
                        LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
                        response.httpstatuscode = HttpStatusCode.Unauthorized;
                        response.errorCode = "UNAUTHORIZED_001";
                        response.message = "Access token is either invalid or has expired.";
                        return response;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return response;
        }


            public ValidateResponse LogError(ValidateResponse response, string message, string errorCode, HttpStatusCode statusCode)
        {

            response.message = message;
            response.errorCode = errorCode;
            response.httpstatuscode = statusCode;
            return response;

        }
        //public async Task<ValidateResponse> ValidateOKTA(JObject requestData, IDictionary<string, string> headerDict)
        //{
        //    ValidateResponse response = new ValidateResponse();
        //    try
        //    {

        //        if (!string.IsNullOrEmpty(_accessToken))
        //        {
        //            string tokenResponse = await _oktaClient.PostAsync(Environment.GetEnvironmentVariable("OKTA_ClientID"),
        //                                                            Environment.GetEnvironmentVariable("OKTA_ClientSecret"),
        //                                                            Environment.GetEnvironmentVariable("OKTA_validationURL"), _accessToken, headerDict);

        //            TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);


        //            if (token.uid != null)
        //            {
        //                System.Net.Http.HttpResponseMessage oktaUserResponse = await _httpClient.GetAsync(Environment.GetEnvironmentVariable("OKTA_UserProfile") + "/" + token.uid, null, Environment.GetEnvironmentVariable("OKTA_ApiKey"), Environment.GetEnvironmentVariable("OKTA_keyType"));
        //                string responseContent = await oktaUserResponse.Content.ReadAsStringAsync();

        //                OktaUser oktaUser = JsonConvert.DeserializeObject<OktaUser>(responseContent);

        //                if (oktaUser.profile.organization.ToLower() == headerDict["X-Organization"].ToLower())
        //                {
        //                    //if (oktaUser.profile.partyId.ToLower() == headerDict["X-Party-Id"].ToLower())
        //                    //{
        //                    //    //if (oktaUser.profile.userType.ToLower() == "service")
        //                    //    //{

        //                    //    //    return LogError(response, Environment.GetEnvironmentVariable("FORBIDDEN_003_Message"), Environment.GetEnvironmentVariable("FORBIDDEN_003"), HttpStatusCode.Forbidden);

        //                    //    //}

        //                    //}
        //                    //else
        //                    //{
        //                    //    LogError(response, "CONFIGERROR_000", "CONFIGERROR_000", HttpStatusCode.Forbidden);
        //                    //    response.errorCode = "CONFIGERROR_000";
        //                    //    response.message = "The server has an internal configuration error: Missing configuration setting. Please contact Aspire Administrator.";
        //                    //    return response;

        //                    //}
        //                }
        //                else
        //                {

        //                    LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
        //                    response.errorCode = "UNAUTHORIZED_001";
        //                    response.message = "Access token is either invalid or has expired.";
        //                    return response;
        //                }
        //            }
        //            else
        //            {
        //                LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
        //                response.errorCode = "UNAUTHORIZED_001";
        //                response.message = "Access token is either invalid or has expired.";
        //                return response;
        //            }
        //        }
        //        else
        //        {
        //            LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
        //            response.errorCode = "FORBIDDEN_004";
        //            response.message = "Authorization header is missing or not of type Bearer access_token.";
        //            return response;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        LogError(response, Environment.GetEnvironmentVariable("UnAuthorised_Message_001"), Environment.GetEnvironmentVariable("UnAuthorised_001"), HttpStatusCode.Unauthorized);
        //        //response.errorCode = "Internal Server Error";
        //        response.message = "Internal Server Error";
        //        return response;
        //    }
        //    return response;
        //}

        //public async Task<ValidateResponse> validate(string clientID, string requestBody, string accessToken, IDictionary<string, string> headerDict, ExecutionContext executionContext, string transactionType)
        //{

        //    ValidateResponse response = new ValidateResponse();
        //    try
        //    {
        //        JObject requestData = JsonConvert.DeserializeObject<JObject>(requestBody);



        //        if (string.IsNullOrEmpty(_clientID))
        //        {

        //            return LogError(response, Environment.GetEnvironmentVariable("INVALIDORG_000_Message"), Environment.GetEnvironmentVariable("INVALIDORG_000"), HttpStatusCode.BadRequest);
        //        }
        //        //if (string.IsNullOrEmpty(headerDict["X-Party-Id"]))
        //        //{
        //        //    return LogError(response, Environment.GetEnvironmentVariable("INVALIDParty_000_Message"), Environment.GetEnvironmentVariable("INVALIDParty_000"), HttpStatusCode.BadRequest);
        //        //}

        //        ValidateResponse isAccessTokenValid = await ValidateOKTA(requestData, headerDict);
        //        if (isAccessTokenValid.message != null)
        //        {

        //            return isAccessTokenValid;

        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        return LogError(response, ex.Message, Environment.GetEnvironmentVariable("INTERNALERROR_000"), HttpStatusCode.InternalServerError);
        //    }
        //    return response;

        //}


        //public async Task<ValidateResponse> ValidateSearchRequest(IDictionary<string, string> headers)
        //{
        //    ValidateResponse response = new ValidateResponse();
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_clientID))
        //        {
        //            return LogError(response, Environment.GetEnvironmentVariable("INVALIDORG_000_Message"), Environment.GetEnvironmentVariable("INVALIDORG_000"), HttpStatusCode.BadRequest);
        //        }
        //        if (!string.IsNullOrEmpty(_accessToken))
        //        {
        //            string tokenResponse = await _oktaClient.PostAsync(Environment.GetEnvironmentVariable("OKTA_ClientID"),
        //                                                            Environment.GetEnvironmentVariable("OKTA_ClientSecret"),
        //                                                            Environment.GetEnvironmentVariable("OKTA_validationURL"), _accessToken, headers);

        //            TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);


        //            if (token.uid != null)
        //            {
        //                System.Net.Http.HttpResponseMessage oktaUserResponse = await _httpClient.GetAsync(Environment.GetEnvironmentVariable("OKTA_UserProfile") + "/" + token.uid, null, Environment.GetEnvironmentVariable("OKTA_ApiKey"), Environment.GetEnvironmentVariable("OKTA_keyType"));
        //                string responseContent = await oktaUserResponse.Content.ReadAsStringAsync();

        //                OktaUser oktaUser = JsonConvert.DeserializeObject<OktaUser>(responseContent);

        //                if (oktaUser.profile.organization.ToLower() == _clientID.ToLower())
        //                {
        //                    if (oktaUser.profile.userType.ToLower() == "service")
        //                    {
        //                        LogError(response, "FORBIDDEN_003", "FORBIDDEN_003", HttpStatusCode.Forbidden);
        //                        response.httpstatuscode = HttpStatusCode.Forbidden;
        //                        response.errorCode = "FORBIDDEN_003";
        //                        response.message = "Prohibited account type: Service.";
        //                        return response;
        //                    }
        //                }
        //                else
        //                {
        //                    LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
        //                    response.httpstatuscode = HttpStatusCode.Unauthorized;
        //                    response.errorCode = "UNAUTHORIZED_001";
        //                    response.message = "Access token is either invalid or has expired.";
        //                    return response;
        //                }
        //            }
        //            else
        //            {                        
        //                LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
        //                response.httpstatuscode = HttpStatusCode.Unauthorized;
        //                response.errorCode = "UNAUTHORIZED_001";
        //                response.message = "Access token is either invalid or has expired.";
        //                return response;
        //            }
        //        }
        //        else
        //        {
        //            LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Forbidden);
        //            response.httpstatuscode = HttpStatusCode.Forbidden;
        //            response.errorCode = "FORBIDDEN_004";
        //            response.message = "Authorization header is missing or not of type Bearer access_token.";
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return LogError(response, ex.Message, Environment.GetEnvironmentVariable("INTERNALERROR_000"), HttpStatusCode.InternalServerError);
        //    }
        //    return response;


        //}




        //public ValidateResponse LogError(ValidateResponse response, string message, string errorCode, HttpStatusCode statusCode)
        //{

        //    response.message = message;
        //    response.errorCode = errorCode;
        //    response.httpstatuscode = statusCode;
        //    return response;

        //}

        public async Task<ValidateResponse> ValidateOKTASearch(IDictionary<string, string> headerDict)
        {
            ValidateResponse response = new ValidateResponse();
            try
            {
                string[] DefaultClientsList = _configurationServices.DefaultValidationClients.Split(",");
                string OktaURL = string.Empty;
                if (DefaultClientsList.Contains(headerDict["X-Organization"].ToUpper()))
                {
                    OktaURL = _configurationServices.DefaultIntrospectUrl;
                }
                else
                {
                    OktaURL = _configurationServices.IntrospectUrl;
                }

                if (!string.IsNullOrEmpty(_accessToken))
                {
                    string tokenResponse = await _oktaClient.PostAsync(_configurationServices.IntrospectClientId,
                        _configurationServices.IntrospectClientSecret, OktaURL, _accessToken, headerDict);

                    TokenValidate token = JsonConvert.DeserializeObject<TokenValidate>(tokenResponse);

                    if (token.uid != null)
                    {

                    }
                    else
                    {
                        LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Unauthorized);
                        response.httpstatuscode = HttpStatusCode.Unauthorized;
                        response.errorCode = "UNAUTHORIZED_001";
                        response.message = "Access token is either invalid or has expired.";
                        return response;

                    }
                }
                else
                {
                    LogError(response, "UNAUTHORIZED_001", "UNAUTHORIZED_001", HttpStatusCode.Forbidden);
                    response.httpstatuscode = HttpStatusCode.Unauthorized;
                    response.errorCode = "FORBIDDEN_004";
                    response.message = "Authorization header is missing or not of type Bearer access_token.";
                    return response;
                }
            }
            catch(Exception ex)
            {

            }
            return response;
        }
     

    }
}
