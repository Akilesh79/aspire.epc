﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Aspire.Dining.Reservation.Exp.Selfserve.Utility
{
    public class ValidateResponse
    {

        public string errorCode { get; set; }
        public string partyId { get; set; }
        public string message { get; set; }
        public HttpStatusCode httpstatuscode { get; set; }

    }
    public class ResponseMessage
    {
        public string errorCode { get; set; }
        public string message { get; set; }
        public HttpStatusCode statusCode { get; set; }
    }
    public class ResponseMessageNew
    {
        public string errorCode { get; set; }
        public string message { get; set; }
    }
    public class ResponseMessageCancel
    {
        public string errorCode { get; set; }
        public string message { get; set; }
        public Guid correlationId { get; set; }
    }

    public class ResponseStatus
    {
        public HttpStatusCode status { get; set; }
    }
    public class TokenValidate
    {
        public bool active { get; set; }
        public string scope { get; set; }
        public string username { get; set; }
        public int exp { get; set; }
        public int iat { get; set; }
        public string sub { get; set; }
        public string aud { get; set; }
        public string iss { get; set; }
        public string jti { get; set; }
        public string token_type { get; set; }
        public string client_id { get; set; }
        public string uid { get; set; }
    }
}
