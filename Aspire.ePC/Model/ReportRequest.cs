﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aspire.ePC.Model
{
    public class ReportRequest
    {
        public string correlationId { get; set; }
        public string source { get; set; }
        public string requestedDateTime { get; set; }
        public string organization { get; set; }
        public string programId { get; set; }
        public string partyId { get; set; }
        public string status { get; set; }
        public Requestdetails requestDetails { get; set; }
    }

    public class Requestdetails
    {
        public Customer customer { get; set; }

        public Case _case { get; set; }
        public string status { get; set; }
        public string requestDate { get; set; }
        public Error error { get; set; }
        public Successmessage successMessage { get; set; }
    }

    public class Customer
    {
        public string userName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Verificationdata verificationData { get; set; }
    }

    public class Verificationdata
    {
        public string verificationData { get; set; }
    }

    public class Case
    {
        public string caseType { get; set; }
        public string apiReferenceId { get; set; }
        public string subClient { get; set; }
        public string requestDetails { get; set; }
    }

    public class Error
    {
        public string errorCode { get; set; }
        public string errorDescription { get; set; }
    }

    public class Successmessage
    {
        public string message { get; set; }
    }
}
