﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aspire.ePC.Model
{
    public class ePCCase
    {
        public string clientID { get; set; }
        public string programID { get; set; }
        public string caseID { get; set; }
        public string issueSeq { get; set; }
        public string RequestType { get; set; }
        public string TravelType { get; set; }
        public string AirportCode { get; set; }
        public string CarrierCode { get; set; }
        public string BookingStatus { get; set; }
        public string country { get; set; }
        public string carrierNumber { get; set; }
        public string dateTime { get; set; }
        public string CountryCity { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string passengerDetails { get; set; }
        public string ActualDatetime { get; set; }
        public string passengerCount { get; set; }
        public string totalCustomerHire { get; set; }
        public string vehicleType { get; set; }
        public string requestReferenceId { get; set; }
        public string resturantName { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string contentID { get; set; }
        public string localCurrency { get; set; }
        public string totalcustomerechargeHire { get; set; }
        public string baseCurrency { get; set; }
        public string carrierNumbercarrierCode { get; set; }
        public string childSeat { get; set; }
        public string babySeat { get; set; }
        public string numberOfBags { get; set; }
        public string Notes { get; set; }
        public string BookerName { get; set; }
        public string case_text { get; set; }
    }
}