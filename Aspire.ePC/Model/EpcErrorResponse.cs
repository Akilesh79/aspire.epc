﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Aspire.ePC.Model
{


    public class EpcErrorResponse : Response
    {
        public HttpStatusCode httpstatuscode { get; set; }
    }


    public class Response
    {
        public string message { get; set; }
        public string errorCode { get; set; }
    }

    public class ErrorInformation
    {

        public string errorCode { get; set; }
        public string errorMessage { get; set; }
        public string clientId { get; set; }
        public string endPoint { get; set; }
    }

}
