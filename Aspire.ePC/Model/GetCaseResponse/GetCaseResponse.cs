﻿namespace Aspire.ePC.Model.GetCaseResponse
{

    public class GetCaseResponse
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
        public Case Case { get; set; }
        public Responseformat ResponseFormat { get; set; }
    }

    public class Case
    {
        public string company_id { get; set; }
        public string case_id { get; set; }
        public string b19_code { get; set; }
        public Addresslist AddressList { get; set; }
    }

    public class Addresslist
    {
        public Address[] Address { get; set; }
    }

    public class Address
    {
         public string a62_code { get; set; }
    }

    public class Responseformat
    {
        public Caselist CaseList { get; set; }
    }

    public class Caselist
    {
        public Case1 Case { get; set; }
    }

    public class Case1
    {
        public string AllAttributes { get; set; }
        public Addresslist1 AddressList { get; set; }
        public IssueList1 IssueList { get; set; }
        public SurveyResultList SurveyResultList { get; set; }
        public OtherAddressList OtherAddressList { get; set; }
        public CaseTextList CaseTextList { get; set; }
         public CaseEnclosureList CaseEnclosureList { get; set; }
         public ActionList ActionList { get; set; }
        public CaseAttachmentList CaseAttachmentList { get; set; }
        public LetterList LetterList { get; set; }
    }

    public class SurveyResultList
    {
        public SurveyResult SurveyResult { get; set; }
    }
    public class SurveyResult
    {
        public string AllAttributes { get; set; }
    }

    public class OtherAddressList
    {
        public OtherAddress OtherAddress { get; set; }
    }
    public class OtherAddress
    {
        public string AllAttributes { get; set; }
    }

    public class CaseTextList
    {
        public CaseText CaseText { get; set; }
    }
    public class CaseText
    {
        public string AllAttributes { get; set; }
    }

    public class CaseEnclosureList
    {
        public CaseEnclosure CaseEnclosure { get; set; }
    }
    public class CaseEnclosure
    {
        public string AllAttributes { get; set; }
    }

    public class ActionList
    {
        public Action Action { get; set; }
    }
    public class Action
    {
        public string AllAttributes { get; set; }
    }

    public class CaseAttachmentList
    {
        public CaseAttachment CaseAttachment { get; set; }
    }
    public class CaseAttachment
    {
        public string AllAttributes { get; set; }
    }

    public class LetterList
    {
        public Letter Letter { get; set; }
    }
    public class Letter
    {
        public string AllAttributes { get; set; }
    }



    public class IssueList1
    {
        public Issue1 Issue { get; set; }
    }

    public class Issue1
    {
        public string AllAttributes { get; set; }
       // public IssueDetailList[] IssueDetailList { get; set; }
    }

    //public class IssueDetailList
    //{
    //    public IssueDetail IssueDetail { get; set; }
    //}
    //public class IssueDetail
    //{
    //    public string AllAttributes { get; set; }
    //}



    public class Addresslist1
    {
        public Address1 Address { get; set; }
    }

    public class Address1
    {
        public string AllAttributes { get; set; }
        public Phonelist[] PhoneList { get; set; }
    }

    public class Phonelist
    {
        public Phone Phone { get; set; }
    }

    public class Phone
    {
        public string AllAttributes { get; set; }
    }

}
