﻿namespace Aspire.ePC.Model.Epc
{
    public class EpcCreateCase
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Case1[] Case { get; set; }
        public Responseformat ResponseFormat { get; set; }
    }

    public class Responseformat
    {
        public Caselist CaseList { get; set; }
    }

    public class Caselist
    {
        public Case Case { get; set; }
    }

    public class Case
    {
        public string AllAttributes { get; set; }
        public Addresslist AddressList { get; set; }
        public Casetextlist CaseTextList { get; set; }
        public Issuelist IssueList { get; set; }
    }

    public class Addresslist
    {
        public Address Address { get; set; }
    }

    public class Address
    {
        public string AllAttributes { get; set; }
        public Phonelist[] PhoneList { get; set; }
    }

    public class Phonelist
    {
        public Phone Phone { get; set; }
    }

    public class Phone
    {
        public string AllAttributes { get; set; }
    }

    public class Casetextlist
    {
        public Casetext CaseText { get; set; }
    }

    public class Casetext
    {
        public string AllAttributes { get; set; }
    }

    public class Issuelist
    {
        public Issue Issue { get; set; }
    }

    public class Issue
    {
        public string AllAttributes { get; set; }
    }

    public class Case1
    {
        public string case_id { get; set; }
        public string company_id { get; set; }
        public string address_id { get; set; }
        public string initial_user_code { get; set; }
        public string responsible_user_code { get; set; }
        public string case_status { get; set; }
        public string case_status_code { get; set; }
        public string contact_multiplier { get; set; }
        public string first_pick_user_code { get; set; }
        public string first_response_user_code { get; set; }
        public string closed_user_code { get; set; }
        public string added_by_user_code { get; set; }
        public string changed_by_user_code { get; set; }
        public string b06_code { get; set; }
        public string b07_code { get; set; }
        public string b10_code { get; set; }
        public string b12_code { get; set; }
        public string b13_code { get; set; }
        public string b14_code { get; set; }
        public string b15_code { get; set; }
        public string b16_code { get; set; }
        public string b17_code { get; set; }
        public string b18_code { get; set; }
        public string b19_code { get; set; }
        public string b47_code { get; set; }
        public string b49_code { get; set; }
        public Issuelist1 IssueList { get; set; }
        public Addresslist1 addressList { get; set; }
        public CaseTextList1 caseTextList { get; set; }
        //public OtherAddressList otherAddressList { get; set; }
        //public CaseAttachmentList caseAttachmentList { get; set; }
        //public CaseEnclosureList caseEnclosureList { get; set; }
        //public LetterList letterList { get; set; }
        //public ActionList actionList { get; set; }
        //public SurveyResultList surveyResultList { get; set; }

        /*dynamic fields*/
        public string received { get; set; }
        public string closed { get; set; }
        public string total_work_time { get; set; }
        public string incoming_phone { get; set; }
        public string first_pick_date { get; set; }
        public string first_pick_queue_user_code { get; set; }
        public string first_pick_time { get; set; }
        public string queue_time { get; set; }
        public string queue_count { get; set; }
        public string transfer_count { get; set; }
        public string first_response_date { get; set; }
        public string first_response_time { get; set; }
        public string communication_count { get; set; }
        public string closed_time { get; set; }
        public string pci_status { get; set; }
        public string case_address_role { get; set; }
        public string date_added { get; set; }
        public string date_changed { get; set; }
        public string agent_assist { get; set; }
        public string identity_id { get; set; }

        public string b05_code { get; set; }
        public string b08_code { get; set; }
        public string b09_code { get; set; }
        public string b11_code { get; set; }
        public string b20_code { get; set; }
        public string b21_code { get; set; }
        public string b22_code { get; set; }
        public string b23_code { get; set; }
        public string b24_code { get; set; }
        public string b25_code { get; set; }
        public string b26_code { get; set; }
        public string b27_code { get; set; }
        public string b28_code { get; set; }
        public string b29_code { get; set; }
        public string b30_code { get; set; }
        public string b31_code { get; set; }
        public string b32_code { get; set; }
        public string b33_code { get; set; }
        public string b34_code { get; set; }
        public string b35_code { get; set; }
        public string b36_code { get; set; }
        public string b37_code { get; set; }
        public string b38_code { get; set; }
        public string b39_code { get; set; }
        public string b40_code { get; set; }
        public string b41_code { get; set; }
        public string b42_code { get; set; }
        public string b43_code { get; set; }
        public string b44_code { get; set; }
        public string b45_code { get; set; }
        public string b46_code { get; set; }
        public string b48_code { get; set; }
    }

    public class Issuelist1
    {
        public string keys { get; set; }
        public Issue1[] Issue { get; set; }
    }

    public class Issue1
    {
        public string issue_seq { get; set; }
        public string product_code { get; set; }
        public string assigned_to_user_code { get; set; }
        public string issue_status { get; set; }
        public string issue_status_code { get; set; }
        public string c05_code { get; set; }
        public string c06_code { get; set; }
        public string c07_code { get; set; }
        public string c08_code { get; set; }
        public string c09_code { get; set; }
        public string c10_code { get; set; }
        public string c11_code { get; set; }
        public string c12_code { get; set; }
        public string c13_code { get; set; }
        public string c14_code { get; set; }
        public string c15_code { get; set; }
        public string c16_code { get; set; }
        public string c17_code { get; set; }
        public string c18_code { get; set; }
        public string c19_code { get; set; }
        public string c20_code { get; set; }
        public string c21_code { get; set; }
        public string c22_code { get; set; }
        public string c23_code { get; set; }
        public string c24_code { get; set; }
        public string c25_code { get; set; }
        public string c26_code { get; set; }
        public string c27_code { get; set; }
        public string c28_code { get; set; }
        public string c29_code { get; set; }
        public string c37_code { get; set; }
        public string c38_code { get; set; }
        public string c39_code { get; set; }
        public string c40_code { get; set; }
        public string c41_code { get; set; }
        public string c42_code { get; set; }
        public string c43_code { get; set; }
        public string c44_code { get; set; }
        public string c45_code { get; set; }
        public string c46_code { get; set; }
        public string c47_code { get; set; }
        public string c48_code { get; set; }
        public string c49_code { get; set; }
        public string c50_code { get; set; }
        public string c57_code { get; set; }
        public string c58_code { get; set; }
        public string c59_code { get; set; }
        public string c60_code { get; set; }
        public string c61_code { get; set; }
        public string c62_code { get; set; }
        public string c68_code { get; set; }
        public string c91_code { get; set; }
        public string c96_code { get; set; }
        public string c98_code { get; set; }
        public string c99_code { get; set; }

        /*dynamic fields for epc*/
        public string closed { get; set; }
        public string issue_multiplier { get; set; }
        public string custom_date1 { get; set; }
        public string date_added { get; set; }
        public string added_by_user_code { get; set; }
        public string date_changed { get; set; }
        public string changed_by_user_code { get; set; }
        //public string c16_code { get; set; }
        public string c30_code { get; set; }
        public string c31_code { get; set; }
        public string c32_code { get; set; }
        public string c33_code { get; set; }
        public string c34_code { get; set; }
        public string c35_code { get; set; }
        public string c36_code { get; set; }
        public string c51_code { get; set; }
        public string c52_code { get; set; }
        public string c53_code { get; set; }
        public string c54_code { get; set; }
        public string c55_code { get; set; }
        public string c56_code { get; set; }
        public string c63_code { get; set; }
        public string c64_code { get; set; }
        public string c65_code { get; set; }
        public string c66_code { get; set; }
        public string c67_code { get; set; }
        public string c69_code { get; set; }
        public string c70_code { get; set; }
        public string c71_code { get; set; }
        public string c72_code { get; set; }
        public string c73_code { get; set; }
        public string c74_code { get; set; }
        public string c75_code { get; set; }
        public string c76_code { get; set; }
        public string c77_code { get; set; }
        public string c78_code { get; set; }
        public string c79_code { get; set; }
        public string c80_code { get; set; }
        public string c81_code { get; set; }
        public string c82_code { get; set; }
        public string c83_code { get; set; }
        public string c84_code { get; set; }
        public string c85_code { get; set; }
        public string c86_code { get; set; }
        public string c87_code { get; set; }
        public string c88_code { get; set; }
        public string c89_code { get; set; }
        public string c90_code { get; set; }
        public string c92_code { get; set; }
        public string c93_code { get; set; }
        public string c94_code { get; set; }
        public string c95_code { get; set; }
        public string c97_code { get; set; }

    }

    public class Addresslist1
    {
        public string keys { get; set; }
        public Address1[] Address { get; set; }
    }
    public class CaseTextList1
    {
        public string keys { get; set; }
        public CaseText1[] caseText { get; set; }
    }
    public class CaseText1
    {
        public string case_text_seq { get; set; }
        public string action_seq { get; set; }
        public string issue_seq { get; set; }
        public string description { get; set; }
        public string text_type_code { get; set; }
        public string case_text { get; set; }
        public string modified_by_user_code { get; set; }
        public string added_by_user_code { get; set; }
        public string last_modified { get; set; }
        public string date_added { get; set; }
        public string identity_id { get; set; }
        public string case_text_seq_sort { get; set; }
        public string last_modified_sort { get; set; }
        public string issue_seq_sort { get; set; }
        public string date_added_sort { get; set; }
        public string identity_id_sort { get; set; }
    }

        public class Address1
    {
        public string address_id { get; set; }
        public string address_type_code { get; set; }
        public string active { get; set; }
        public string given_names { get; set; }
        public string name_title { get; set; }
        public string company_name { get; set; }
        public string middle_initial { get; set; }
        public string last_name { get; set; }
        public string suffix { get; set; }
        public string job_title { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public string email2 { get; set; }
        public string email3 { get; set; }
        public object latitude { get; set; }
        public object longitude { get; set; }
        public string instructions { get; set; }
        public string county { get; set; }
        public string currency_code { get; set; }
        public string allow_survey { get; set; }
        public string a07_code { get; set; }
        public string a62_code { get; set; }
        public string a69_code { get; set; }
        public string a78_code { get; set; }
        public string a08_code { get; set; }
        public string a09_code { get; set; }
        public string a10_code { get; set; }
        public string a13_code { get; set; }
        public string a14_code { get; set; }
        public string a16_code { get; set; }
        public string a18_code { get; set; }
        public string a19_code { get; set; }
        public string a26_code { get; set; }
        public string a28_code { get; set; }
        public string a38_code { get; set; }
        public string a42_code { get; set; }
        public string a44_code { get; set; }
        public string a45_code { get; set; }
        public string a46_code { get; set; }
        public string a48_code { get; set; }
        public string a49_code { get; set; }
        public string a50_code { get; set; }
        public string a51_code { get; set; }
        public string a52_code { get; set; }
        public string a53_code { get; set; }
        public string a54_code { get; set; }
        public string a58_code { get; set; }
        public string a59_code { get; set; }
        public string a63_code { get; set; }
        public string a65_code { get; set; }
        public string a72_code { get; set; }
        public string a75_code { get; set; }
        public string a76_code { get; set; }
        public string a77_code { get; set; }
        public string a79_code { get; set; }

        public Phonelist1 PhoneList { get; set; }

        /*fields whih are not in process layer but shown up in system layer i.e,dynamic */
        // public string email { get; set; }
        // public DateTime last_contact { get; set; }
        //public string accumulated_goodwill { get; set; }
        // public string opt_out { get; set; }
        //public DateTime date_added { get; set; }
        // public string added_by_user_code { get; set; }
        //public DateTime date_changed { get; set; }
        //public string changed_by_user_code { get; set; }
        // public string search_name { get; set; }
        //public string originated_via { get; set; }
        //public DateTime originated_date { get; set; }
        // public DateTime last_modified { get; set; }
        // public string a11_code { get; set; }

        /*preferences*/
        public string a05_code { get; set; }
        public string a15_code { get; set; }
        public string a20_code { get; set; }
        public string a21_code { get; set; }
        public string a22_code { get; set; }
        public string a30_code { get; set; }
        public string a31_code { get; set; }
        public string a36_code { get; set; }
        public string a60_code { get; set; }
        public string a66_code { get; set; }
        public string a67_code { get; set; }
        public string a74_code { get; set; }


        /*membership*/
        public string a24_code { get; set; }
        public string a27_code { get; set; }
        public string a32_code { get; set; }
        public string a34_code { get; set; }
        public string a37_code { get; set; }
        public string a39_code { get; set; }
        public string a41_code { get; set; }
        public string a43_code { get; set; }
        public string a73_code { get; set; }
        public string a56_code { get; set; }
        public string a61_code { get; set; }
        public string a80_code { get; set; }

        /*dynamic fields for "a" list*/

        public string address_code { get; set; }
        public string account_number { get; set; }
        public string email { get; set; }
        public string search_name { get; set; }
        public string search_address { get; set; }
        public string originated_via { get; set; }
        public string originated_date { get; set; }
        public string last_modified { get; set; }
        public string last_survey { get; set; }
        public string last_contact { get; set; }
        public object accumulated_goodwill { get; set; }
        public string where_to_buy { get; set; }
        public string repeater_code { get; set; }
        public object encl_auth_level { get; set; }
        public string opt_out { get; set; }
        public string language_id { get; set; }
        public string primary_address_id { get; set; }
        public string date_added { get; set; }
        public string added_by_user_code { get; set; }
        public string date_changed { get; set; }
         public string current_code { get; set; }
        public string changed_by_user_code { get; set; }
        public string a06_code { get; set; }
        public string a11_code { get; set; }
        public string a12_code { get; set; }
        public string a17_code { get; set; }
        public string a23_code { get; set; }
        public string a25_code { get; set; }
        public string a29_code { get; set; }
        public string a33_code { get; set; }
        public string a35_code { get; set; }
        public string a40_code { get; set; }
        public string a47_code { get; set; }
        public string a55_code { get; set; }
        public string a57_code { get; set; }
        public string a64_code { get; set; }
        public string a68_code { get; set; }
        public string a70_code { get; set; }
        public string a71_code { get; set; }
    }

    public class Phonelist1
    {
        public Phone1[] Phone { get; set; }
    }

    public class Phone1
    {
        public string address_phone_seq { get; set; }
        public string phone_type_code { get; set; }
        public string phone { get; set; }
    }
}
