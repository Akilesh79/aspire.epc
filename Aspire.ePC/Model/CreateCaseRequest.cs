﻿using System.Collections.Generic;

namespace Aspire.ePC.Model
{

    public class IssueList
    {
        public int issueSeq { get; set; }
        public string productCode { get; set; }
        public string assignedToUserCode { get; set; }
        public string issueStatus { get; set; }
        public string issueStatusCode { get; set; }
        public string c05Code { get; set; }
        public string c06Code { get; set; }
        public string c07Code { get; set; }
        public string c08Code { get; set; }
        public string c09Code { get; set; }
        public string c10Code { get; set; }
        public string c11Code { get; set; }
        public string c12Code { get; set; }
        public string c13Code { get; set; }
        public string c14Code { get; set; }
        public string c15Code { get; set; }
        public string c17Code { get; set; }
        public string c18Code { get; set; }
        public string c19Code { get; set; }
        public string c20Code { get; set; }
        public string c21Code { get; set; }
        public string c22Code { get; set; }
        public string c23Code { get; set; }
        public string c24Code { get; set; }
        public string c25Code { get; set; }
        public string c26Code { get; set; }
        public string c27Code { get; set; }
        public string c28Code { get; set; }
        public string c29Code { get; set; }
        public string c37Code { get; set; }
        public string c38Code { get; set; }
        public string c39Code { get; set; }
        public string c40Code { get; set; }
        public string c41Code { get; set; }
        public string c42Code { get; set; }
        public string c43Code { get; set; }
        public string c44Code { get; set; }
        public string c45Code { get; set; }
        public string c46Code { get; set; }
        public string c47Code { get; set; }
        public string c48Code { get; set; }
        public string c49Code { get; set; }
        public string c50Code { get; set; }
        public string c57Code { get; set; }
        public string c58Code { get; set; }
        public string c59Code { get; set; }
        public string c60Code { get; set; }
        public string c61Code { get; set; }
        public string c62Code { get; set; }
        public string c68Code { get; set; }
        public string c91Code { get; set; }
        public string c96Code { get; set; }
        public string c98Code { get; set; }
        public string c99Code { get; set; }

        /*dynamic fields for "c" code list*/

        public string closed { get; set; }
        public int issueMultiplier { get; set; }
        public string customDate1 { get; set; }
        public string dateAdded { get; set; }
        public string addedByUserCode { get; set; }
        public string dateChanged { get; set; }
        public string changedByUserCode { get; set; }


        public string c16Code { get; set; }
        public string c30Code { get; set; }
        public string c31Code { get; set; }
        public string c32Code { get; set; }
        public string c33Code { get; set; }
        public string c34Code { get; set; }
        public string c35Code { get; set; }
        public string c36Code { get; set; }
        public string c51Code { get; set; }
        public string c52Code { get; set; }
        public string c53Code { get; set; }
        public string c54Code { get; set; }
        public string c55Code { get; set; }
        public string c56Code { get; set; }
        public string c63Code { get; set; }
        public string c64Code { get; set; }
        public string c65Code { get; set; }
        public string c66Code { get; set; }
        public string c67Code { get; set; }
        public string c69Code { get; set; }
        public string c70Code { get; set; }
        public string c71Code { get; set; }
        public string c72Code { get; set; }
        public string c73Code { get; set; }
        public string c74Code { get; set; }
        public string c75Code { get; set; }
        public string c76Code { get; set; }
        public string c77Code { get; set; }
        public string c78Code { get; set; }
        public string c79Code { get; set; }
        public string c80Code { get; set; }
        public string c81Code { get; set; }
        public string c82Code { get; set; }
        public string c83Code { get; set; }
        public string c84Code { get; set; }
        public string c85Code { get; set; }
        public string c86Code { get; set; }
        public string c87Code { get; set; }
        public string c88Code { get; set; }
        public string c89Code { get; set; }
        public string c90Code { get; set; }
        public string c92Code { get; set; }
        public string c93Code { get; set; }
        public string c94Code { get; set; }
        public string c95Code { get; set; }
        public string c97Code { get; set; }


    }

    public class PhoneList
    {
        public string addressPhoneSeq { get; set; }
        public string phoneTypeCode { get; set; }
        public string phone { get; set; }
        public string phoneNote { get; set; }
    }

    public class Faxes
    {
        public string addressPhoneSeq { get; set; }
        public string phoneTypeCode { get; set; }
        public string phone { get; set; }
        public string phoneNote { get; set; }
    }
    //public class OtherAddressList { }

    //public class CaseAttachmentList { }
    //public class CaseEnclosureList { }
    //public class LetterList { }
    //public class ActionList { }
    //public class SurveyResultList { }
    public class AddressList
    {

        public int addressId { get; set; }
        public string addressTypeCode { get; set; }
        public bool active { get; set; }
        public string nameTitle { get; set; }
        public string givenNames { get; set; }
        public char middleInitial { get; set; }
        public string lastName { get; set; }
        public string suffix { get; set; }
        public string companyName { get; set; }
        public string jobTitle { get; set; }
        public string instructions { get; set; }
        public string currencyCode { get; set; }
        public string a07Code { get; set; }
        public string a08Code { get; set; }
        public string a09Code { get; set; }
        public string a10Code { get; set; }
        public string a13Code { get; set; }
        public string a14Code { get; set; }
        public string a16Code { get; set; }
        public string a18Code { get; set; }
        public string a19Code { get; set; }
        public string a26Code { get; set; }
        public string a28Code { get; set; }
        public string a38Code { get; set; }
        public string a42Code { get; set; }
        public string a44Code { get; set; }
        public string a45Code { get; set; }
        public string a46Code { get; set; }
        public string a48Code { get; set; }
        public string a49Code { get; set; }
        public string a50Code { get; set; }
        public string a51Code { get; set; }
        public string a52Code { get; set; }
        public string a53Code { get; set; }
        public string a54Code { get; set; }
        public string a58Code { get; set; }
        public string a59Code { get; set; }
        public string a62Code { get; set; }
        public string a63Code { get; set; }
        public string a65Code { get; set; }
        public string a69Code { get; set; }
        public string a72Code { get; set; }
        public string a75Code { get; set; }
        public string a76Code { get; set; }
        public string a77Code { get; set; }
        public string a78Code { get; set; }
        public string a79Code { get; set; }
        public bool allowSurvey { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string county { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string emailTypeId { get; set; }//need to confirm not used in epc calling
        public string email2 { get; set; }
        public string email3 { get; set; }
        //public List<Preferences> preferences { get; set; }
        /*preferences*/
        public string a05Code { get; set; }
        public string a15Code { get; set; }
        public string a20Code { get; set; }
        public string a21Code { get; set; }
        public string a22Code { get; set; }
        public string a30Code { get; set; }
        public string a31Code { get; set; }
        public string a36Code { get; set; }
        public string a60Code { get; set; }
        public string a66Code { get; set; }
        public string a67Code { get; set; }
        public string a74Code { get; set; }
        //public string a77Code { get; set; }//need to confirm why they are repeated
        //public string a26Code { get; set; }
        //public string a58Code { get; set; }
        //public string a45Code { get; set; }
        //public string a46Code { get; set; }
        //public string a38Code { get; set; }
        //public string a28Code { get; set; }
        //public string a13Code { get; set; }

        //public List<Membership> membership { get; set; }
        /*membership*/
        public string a24Code { get; set; }
        public string a27Code { get; set; }
        public string a32Code { get; set; }
        public string a34Code { get; set; }
        public string a37Code { get; set; }
        public string a39Code { get; set; }
        public string a41Code { get; set; }
        public string a43Code { get; set; }
        public string a73Code { get; set; }
        public string a56Code { get; set; }
        public string a61Code { get; set; }
        public string a80Code { get; set; }

        public List<PhoneList> phoneList { get; set; }
        public List<Faxes> faxes { get; set; }


        /*dynamic fields for "a" list*/

        public string addressCode { get; set; }
        public string accountNumber { get; set; }
        public string email { get; set; }
        public string searchName { get; set; }
        public string searchAddress { get; set; }
        public string originatedVia { get; set; }
        public string originatedDate { get; set; }
        public string lastModified { get; set; }
        public string lastSurvey { get; set; }
        public string lastContact { get; set; }
        public double accumulatedGoodwill { get; set; }
        public string whereToBuy { get; set; }
        public string repeaterCode { get; set; }
        public int enclAuthLevel { get; set; }
        public bool optOut { get; set; }
        public string languageId { get; set; }
        public string primaryAddressId { get; set; }
        public string dateAdded { get; set; }
        public string addedByUserCode { get; set; }
        public string dateChanged { get; set; }
        public string changedByUserCode { get; set; }
        public string currentCode { get; set; }
        public string a06Code { get; set; }
        public string a11Code { get; set; }
        public string a12Code { get; set; }
        public string a17Code { get; set; }
        public string a23Code { get; set; }
        public string a25Code { get; set; }
        public string a29Code { get; set; }
        public string a33Code { get; set; }
        public string a35Code { get; set; }
        public string a40Code { get; set; }
        public string a47Code { get; set; }
        public string a55Code { get; set; }
        public string a57Code { get; set; }
        public string a64Code { get; set; }
        public string a68Code { get; set; }
        public string a70Code { get; set; }
        public string a71Code { get; set; }

    }

    public class CreateCaseRequest
    {
        public int caseId { get; set; }
        public string companyId { get; set; }
        public string initialUserCode { get; set; }
        public string responsibleUserCode { get; set; }
        public int addressId { get; set; }
        public string caseStatus { get; set; }
        public string caseStatusCode { get; set; }
        public object contactMultiplier { get; set; }
        public string firstPickUserCode { get; set; }
        public string firstResponseUserCode { get; set; }
        public string closedUserCode { get; set; }
        public string addedByUserCode { get; set; }
        public string changedByUserCode { get; set; }
        public string b06Code { get; set; }
        public string b07Code { get; set; }
        public string b10Code { get; set; }
        public string b12Code { get; set; }
        public string b13Code { get; set; }
        public string b14Code { get; set; }
        public string b15Code { get; set; }
        public string b16Code { get; set; }
        public string b17Code { get; set; }
        public string b18Code { get; set; }
        public string b19Code { get; set; }
        public string b47Code { get; set; }
        public string b49Code { get; set; }
        public List<IssueList> issueList { get; set; }
        public List<AddressList> addressList { get; set; }
        public CaseTextList1 caseTextList { get; set; }
        //public List<OtherAddressList> otherAddressList { get; set; }
        //public List<CaseAttachmentList> caseAttachmentList { get; set; }
        //public List<CaseEnclosureList> caseEnclosureList { get; set; }
        //public List<LetterList> letterList { get; set; }
        //public List<ActionList> actionList { get; set; }
        //public List<SurveyResultList> surveyResultList { get; set; }

        /*Dynamic fields for "b" code list*/

        public string received { get; set; }
        public string closed { get; set; }
        public object totalWorkTime { get; set; }
        public string incomingPhone { get; set; }
        public string firstPickDate { get; set; }
        public string firstPickQueueUserCode { get; set; }
        public object firstPickTime { get; set; }
        public object queueTime { get; set; }
        public object queueCount { get; set; }
        public object transferCount { get; set; }
        public string firstResponseDate { get; set; }
        public object firstResponseTime { get; set; }
        public object communicationCount { get; set; }
        public object closedTime { get; set; }
        public string pciStatus { get; set; }
        public string caseAddressRole { get; set; }
        public string dateAdded { get; set; }
        public string dateChanged { get; set; }
        public string agentAssist { get; set; }
        public object identityId { get; set; }

        public string b05Code { get; set; }
        public string b08Code { get; set; }
        public string b09Code { get; set; }
        public string b11Code { get; set; }
        public string b20Code { get; set; }
        public string b21Code { get; set; }
        public string b22Code { get; set; }
        public string b23Code { get; set; }
        public string b24Code { get; set; }
        public string b25Code { get; set; }
        public string b26Code { get; set; }
        public string b27Code { get; set; }
        public string b28Code { get; set; }
        public string b29Code { get; set; }
        public string b30Code { get; set; }
        public string b31Code { get; set; }
        public string b32Code { get; set; }
        public string b33Code { get; set; }
        public string b34Code { get; set; }
        public string b35Code { get; set; }
        public string b36Code { get; set; }
        public string b37Code { get; set; }
        public string b38Code { get; set; }
        public string b39Code { get; set; }
        public string b40Code { get; set; }
        public string b41Code { get; set; }
        public string b42Code { get; set; }
        public string b43Code { get; set; }
        public string b44Code { get; set; }
        public string b45Code { get; set; }
        public string b46Code { get; set; }
        public string b48Code { get; set; }

    }

    public class CaseTextList1
    {
        public string keys { get; set; }
        public CaseText1[] caseText { get; set; }
    }
    public class CaseText1
    {
        public string case_text_seq { get; set; }
        public string action_seq { get; set; }
        public string issue_seq { get; set; }
        public string description { get; set; }
        public string text_type_code { get; set; }
        public string case_text { get; set; }
        public string modified_by_user_code { get; set; }
        public string added_by_user_code { get; set; }
        public string last_modified { get; set; }
        public string date_added { get; set; }
        public string identity_id { get; set; }
        public string case_text_seq_sort { get; set; }
        public string last_modified_sort { get; set; }
        public string issue_seq_sort { get; set; }
        public string date_added_sort { get; set; }
        public string identity_id_sort { get; set; }
    }

    public class CaseTextList
    {
        public object caseTextSeq { get; set; }
        public object actionSeq { get; set; }
        public object issueSeq { get; set; }
        public string caseText { get; set; }
        public string description { get; set; }
        public string textTypeCode { get; set; }
        public string modifiedByUserCode { get; set; }
        public string addedByUserCode { get; set; }
        public string lastModified { get; set; }
        public string dateAdded { get; set; }
        public string identityId { get; set; }
        public string caseTextSeqSort { get; set; }
        public string lastModifiedSort { get; set; }
        public string issueSeqSort { get; set; }
        public string dateAddedSort { get; set; }
        public string identityIdSort { get; set; }
    }

}
