using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CoreCommon.Contract;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Aspire.ePC.Service;
using Aspire.ePC.Model;
using System.Net;
using System.Net.Http;

namespace Aspire.ePC.Functions
{
    public class ePCCase
    {
        private readonly IHttpClient _httpClient;
        private readonly IOktaToken _oktaClient;
        public IConfiguration _configuration;

        public ePCCase(IHttpClient httpClient, IOktaToken oktaClient)
        {
            _httpClient = httpClient;
            _oktaClient = oktaClient;
        }

        [FunctionName("ePCCase")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            log.LogInformation("ePCCase : " + requestBody);

            IDictionary<string, string> headerDict = new Dictionary<string, string>
            {
                { "X-Organization", req.Headers["X-Organization"].ToString() },
                {"X-Program-Id", req.Headers["X-Program-Id"].ToString() },
                { "clientID", req.Headers["clientID"].ToString() },
                {"issueSeq", req.Headers["issueSeq"].ToString() }
            };

            string clientID = req.Headers["X-Organization"].ToString();

            CaseProcessRequest procRequest = new CaseProcessRequest(requestBody, clientID, _httpClient, req.Method, _oktaClient, _configuration, log, headerDict);
            object response = await procRequest.ProcessCaseBody();


            return new OkObjectResult(response);
        }

        [FunctionName("Cases")]
        public async Task<IActionResult> SelfServeCases(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", "Put", "Delete", Route = "cases")] HttpRequest req,
            ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            string clientID = req.Headers["X-Organization"].ToString();
            string correlationId = req.Headers["X-Correlation-Id"].ToString();
            string programId = req.Headers["X-Program-Id"].ToString();

            log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCases " + " Message:  " + requestBody);

            IDictionary<string, string> headerDict = new Dictionary<string, string>
            {
                { "X-Organization", clientID },
                { "X-Correlation-Id", correlationId },
                {"X-Program-Id", programId }
            };

            CreateCaseRequest requestData = JsonConvert.DeserializeObject<CreateCaseRequest>(requestBody);

            EpcErrorResponse epcErrorResponse = new EpcErrorResponse();

            if (req.Method != HttpMethod.Post.ToString())
            {
                if (requestData.caseId <= 0)
                {
                    epcErrorResponse.errorCode = Environment.GetEnvironmentVariable("ErrorCode");
                    epcErrorResponse.message = Environment.GetEnvironmentVariable("CaseIdError");
                    epcErrorResponse.httpstatuscode = HttpStatusCode.BadRequest;
                    log.LogInformation("###### Correlation ID : " + headerDict["X-Correlation-Id"].ToString() + "|| Method Name: ProcessCaseBody " + " Message:  " + epcErrorResponse.errorCode + " - " + epcErrorResponse.message);

                    Response errResponse = JsonConvert.DeserializeObject<Response>(JsonConvert.SerializeObject(epcErrorResponse));
                    EpcErrorResponse errormessage = JsonConvert.DeserializeObject<EpcErrorResponse>(JsonConvert.SerializeObject(errResponse));
                    log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCases " + " Message:  " + errResponse.message);

                    return new ContentResult
                    {
                        Content = JsonConvert.SerializeObject(errResponse, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                        ContentType = "application/json",
                        StatusCode = errormessage.httpstatuscode.GetHashCode()
                    };
                }
            }

            CaseProcessRequest procRequest = new CaseProcessRequest(requestBody, clientID, _httpClient, req.Method, _oktaClient, _configuration, log, headerDict);
            object response = await procRequest.ProcessCaseBody();

            string responseContent = JsonConvert.SerializeObject(response);

            EpcErrorResponse err = JsonConvert.DeserializeObject<EpcErrorResponse>(responseContent);

            log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCases " + " Message:  " + responseContent.ToString());

            if (err.httpstatuscode == 0)
            {
                return new ContentResult
                {
                    Content = responseContent,
                    ContentType = "application/json",
                    StatusCode = HttpStatusCode.OK.GetHashCode()
                };
            }
            else
            {
                Response errResponse = JsonConvert.DeserializeObject<Response>(responseContent);
                err = JsonConvert.DeserializeObject<EpcErrorResponse>(responseContent);
                log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCases " + " Message:  " + errResponse.message);

                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(errResponse, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                    ContentType = "application/json",
                    StatusCode = err.httpstatuscode.GetHashCode()
                };
            }
        }

        [FunctionName("CasesGet")]
        public async Task<IActionResult> SelfServeCasesGet(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "cases")] HttpRequest req,
           ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            string clientID = req.Query["companyId"].ToString();
            string caseID = req.Query["caseId"].ToString();
            string correlationId = req.Query["X-Correlation-Id"].ToString();
            log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCasesGet " + " Message:  " + requestBody);

            IDictionary<string, string> headerDict = new Dictionary<string, string>
            {
                { "companyId", clientID },
                { "caseId", caseID},
                { "X-Correlation-Id", correlationId }
            };

            CaseProcessRequest procRequest = new CaseProcessRequest(requestBody, clientID, _httpClient, req.Method, _oktaClient, _configuration, log, headerDict);
            object response = await procRequest.ProcessGetCase(clientID, caseID);
            log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCasesGet " + " Message:  " + response.ToString());
            return new OkObjectResult(response);
        }

        [FunctionName("CasesSearch")]
        public async Task<IActionResult> SelfServeCasesSearch(
         [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "cases/search")] HttpRequest req,
         ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            string clientID = req.Query["companyId"].ToString();
            string caseID = req.Query["caseId"].ToString();
            string apiReferenceNumber = req.Query["b19Code"].ToString();
            string partyId = req.Query["a62Code"].ToString();
            string correlationId = req.Query["X-Correlation-Id"].ToString();
            log.LogInformation("###### Correlation ID : " + correlationId?.ToString() + "|| Method Name: SelfServeCasesSearch " + " Message:  " + requestBody);
            IDictionary<string, string> headerDict = new Dictionary<string, string>
            {
                { "companyId", clientID },
                { "caseId", caseID},
                { "X-Correlation-Id", correlationId }
            };

            CaseProcessRequest procRequest = new CaseProcessRequest(_httpClient, headerDict, log, _configuration);
            object response = await procRequest.SearchCase(clientID, caseID, apiReferenceNumber, partyId);

            log.LogInformation("###### Correlation ID : " + correlationId + "|| Method Name: SelfServeCasesSearch " + " Message:  " + response.ToString());
            return new OkObjectResult(JsonConvert.SerializeObject(response, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
        }
    }
}
